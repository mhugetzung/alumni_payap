<?php
require_once("../../pages/auth.inc.php");

if(isset($_POST)) {
    $dep_name = $_POST["dep_name"];
    $dep_name_en = $_POST["dep_name_en"];

    $sql = "INSERT INTO department (dep_name,dep_name_en) 
                VALUES ('$dep_name', '$dep_name_en')";
    $result = mysqli_query($conn, $sql) or die("Error in query: $sql " . mysqli_error($conn));

    echo "<script>";
    echo "alert('เพิ่มข้อมูลเรียบร้อย')";
    echo "</script>";

    header("Refresh:0; url=../../pages/department/create.php");
} else {
    header("Location: ../../pages/department/create.php");
}
mysqli_close($conn);