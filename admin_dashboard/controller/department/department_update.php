<?php
require_once("../../pages/auth.inc.php");

if(isset($_GET['dep_id']) && isset($_POST)) {
    $dep_id = $_GET['dep_id'];
    $dep_name = $_POST["dep_name"];
    $dep_name_en = $_POST["dep_name_en"];

    $sql = "UPDATE department
            SET dep_name = '$dep_name',
            dep_name_en = '$dep_name_en'
            WHERE dep_id = '$dep_id'";
    $result = mysqli_query($conn, $sql) or die("Error in query: $sql " . mysqli_error($conn));

    echo "<script>";
    echo "alert('แก้ไขข้อมูลเรียบร้อย')";
    echo "</script>";

    header("Refresh:0; url=../../pages/department/index.php?dep_id=$dep_id");
} else {
    header("Location: ../../pages/department/edit.php");
}
mysqli_close($conn);