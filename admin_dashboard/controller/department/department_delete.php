<?php
require_once("../../pages/auth.inc.php");

if(isset($_GET['dep_id'])) {
    $dep_id = $_GET['dep_id'];

    $sql = "DELETE FROM department WHERE dep_id = '$dep_id'";
    $result = mysqli_query($conn, $sql) or die("Error in query: $sql " . mysqli_error($conn));

    echo "<script>";
    echo "alert('ลบข้อมูลเรียบร้อย')";
    echo "</script>";

    header("Refresh:0; url=../../pages/department/index.php");
} else {
    header("Location: ../../pages/department/index.php");
}
mysqli_close($conn);