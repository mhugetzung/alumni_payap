<?php
require_once("../../pages/auth.inc.php");

if (isset($_GET['inf_id']) && isset($_POST)) {
    $inf_id = $_GET['inf_id'];
    $inf_ti1 = $_POST["inf_ti1"];
    $inf_ti1_en = $_POST["inf_ti1_en"];
    $inf_det1 = $_POST["inf_det1"];
    $inf_det1_en = $_POST["inf_det1_en"];
    $inf_det2 = $_POST["inf_det2"] ?? '';
    $inf_det2_en = $_POST["inf_det2_en"] ?? '';
    $inf_det3 = $_POST["inf_det3"] ?? '';
    $inf_det3_en = $_POST["inf_det3_en"] ?? '';
    $inf_ti2 = $_POST["inf_ti2"];
    $inf_ti2_en = $_POST["inf_ti2_en"];
    $inf_det4 = $_POST["inf_det4"];
    $inf_det4_en = $_POST["inf_det4_en"];
    $inf_det5 = $_POST["inf_det5"] ?? '';
    $inf_det5_en = $_POST["inf_det5_en"] ?? '';
    $inf_det6 = $_POST["inf_det6"] ?? '';
    $inf_det6_en = $_POST["inf_det6_en"] ?? '';
    $inf_updated = date('y-m-d H:i:s');
    $admin_id = $_SESSION["admin_id"];
    $inf_img = $_FILES["inf_img"]['name'];
    $img_tmp = $_FILES['inf_img']['tmp_name'];

    move_uploaded_file($img_tmp, "../../../images/information/$inf_img");

    $sql = "UPDATE information
            SET inf_ti1 = '$inf_ti1',
                inf_ti1_en = '$inf_ti1_en',
                inf_det1 = '$inf_det1',
                inf_det1_en = '$inf_det1_en',
                inf_det2 = '$inf_det2',
                inf_det2_en = '$inf_det2_en',
                inf_det3 = '$inf_det3',
                inf_det3_en = '$inf_det3_en',
                inf_ti2 = '$inf_ti2',
                inf_ti2_en = '$inf_ti2_en',
                inf_det4 = '$inf_det4',
                inf_det4_en = '$inf_det4_en',
                inf_det5 = '$inf_det5',
                inf_det5_en = '$inf_det5_en',
                inf_det6 = '$inf_det6',
                inf_det6_en = '$inf_det6_en',
                inf_img = '$inf_img',
                inf_updated = '$inf_updated'
                
            WHERE inf_id = '$inf_id'";
    $result = mysqli_query($conn, $sql) or die("Error in query: $sql " . mysqli_error($conn));

    echo "<script>";
    echo "alert('แก้ไขข้อมูลเรียบร้อย')";
    echo "</script>";

    header("Refresh:0; url=../../pages/dashboard/index.php");
} else {
    echo "<script>";
    echo "alert('เกิดข้อผิดพลาดในการแก้ไขข้อมูล')";
    echo "</script>";
    header("Location: ../../pages/information/edit.php?inf_id=1");
}
mysqli_close($conn);
