<?php
require_once("../../pages/auth.inc.php");

if (isset($_POST)) {
    $inf_ti1 = $_POST["inf_ti1"];
    $inf_ti1_en = $_POST["inf_ti1_en"];
    $inf_det1 = $_POST["inf_det1"];
    $inf_det1_en = $_POST["inf_det1_en"];
    $inf_det2 = $_POST["inf_det2"] ?? '';
    $inf_det2_en = $_POST["inf_det2_en"] ?? '';
    $inf_det3 = $_POST["inf_det3"] ?? '';
    $inf_det3_en = $_POST["inf_det3_en"] ?? '';
    $inf_ti2 = $_POST["inf_ti2"];
    $inf_ti2_en = $_POST["inf_ti2_en"];
    $inf_det4 = $_POST["inf_det4"];
    $inf_det4_en = $_POST["inf_det4_en"];
    $inf_det5 = $_POST["inf_det5"] ?? '';
    $inf_det5_en = $_POST["inf_det5_en"] ?? '';
    $inf_det6 = $_POST["inf_det6"] ?? '';
    $inf_det6_en = $_POST["inf_det6_en"] ?? '';
    $inf_created = date('y-m-d H:i:s');
    $admin_id = $_SESSION["admin_id"];
    $inf_img = $_FILES["inf_img"]['name'];
    $img_tmp = $_FILES['inf_img']['tmp_name'];

    move_uploaded_file($img_tmp, "../../../images/information/$inf_img");

    $sql = "INSERT INTO information (inf_ti1, inf_ti1_en, inf_det1, inf_det1_en, inf_det2, inf_det2_en,
                inf_det3, inf_det3_en, inf_ti2, inf_ti2_en, inf_det4, inf_det4_en
                , inf_det5, inf_det5_en, inf_det6, inf_det6_en, inf_created, inf_img, admin_id) 
                VALUES ('$inf_ti1', '$inf_ti1_en', '$inf_det1', '$inf_det1_en', '$inf_det2', '$inf_det2_en',
                '$inf_det3', '$inf_det3_en', '$inf_ti2', '$inf_ti2_en', '$inf_det4', '$inf_det4_en'
                , '$inf_det5', '$inf_det5_en', '$inf_det6', '$inf_det6_en', '$inf_created', '$inf_img', '$admin_id')";
    $result = mysqli_query($conn, $sql) or die("Error in query: $sql " . mysqli_error($conn));

    echo "<script>";
    echo "alert('เพิ่มข้อมูลเรียบร้อย')";
    echo "</script>";

    header("Refresh:0; url=../../pages/information/index.php");
} else {
    header("Location: ../../pages/information/create.php");
}
mysqli_close($conn);
