<?php
require_once("../../pages/auth.inc.php");

if(isset($_GET['pro_id']) && isset($_POST)) {
    $pro_id = $_GET['pro_id'];
    $pro_name_th = $_POST["pro_name_th"];
    $pro_name_eng = $_POST["pro_name_eng"];
    $pro_level = $_POST["pro_level"];
    $fac_id = $_POST["fac_id"];
    
    $sql = "UPDATE program 
            SET pro_name_th = '$pro_name_th',
                pro_name_eng = '$pro_name_eng',
                pro_level = '$pro_level',
                fac_id = '$fac_id'
            WHERE pro_id = '$pro_id'";
    $result = mysqli_query($conn, $sql) or die("Error in query: $sql " . mysqli_error($conn));

    echo "<script>";
    echo "alert('แก้ไขข้อมูลเรียบร้อย')";
    echo "</script>";

    header("Refresh:0; url=../../pages/program/index.php?pro_id=$pro_id");
} else {
    header("Location: ../../pages/program/edit.php");
}
mysqli_close($conn);