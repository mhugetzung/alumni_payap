<?php
require_once("../../pages/auth.inc.php");

if(isset($_GET['pro_id'])) {
    $pro_id = $_GET['pro_id'];

    $sql = "DELETE FROM program WHERE pro_id = '$pro_id'";
    $result = mysqli_query($conn, $sql) or die("Error in query: $sql " . mysqli_error($conn));

    echo "<script>";
    echo "alert('ลบข้อมูลเรียบร้อย')";
    echo "</script>";

    header("Refresh:0; url=../../pages/program/index.php");
} else {
    header("Location: ../../pages/program/index.php");
}
mysqli_close($conn);