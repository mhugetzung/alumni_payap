<?php
require_once("../../pages/auth.inc.php");

if(isset($_GET['fac_id']) && isset($_POST)) {
    $fac_id = $_GET['fac_id'];
    $fac_name = $_POST["fac_name"];
    $fac_name_en = $_POST["fac_name_en"];
    

    $sql = "UPDATE faculty
            SET fac_name = '$fac_name',
            fac_name_en = '$fac_name_en'
            WHERE fac_id = '$fac_id'";
    $result = mysqli_query($conn, $sql) or die("Error in query: $sql " . mysqli_error($conn));

    echo "<script>";
    echo "alert('แก้ไขข้อมูลเรียบร้อย')";
    echo "</script>";

    header("Refresh:0; url=../../pages/faculty/index.php?dep_id=$fac_id");
} else {
    header("Location: ../../pages/faculty/edit.php");
}
mysqli_close($conn);