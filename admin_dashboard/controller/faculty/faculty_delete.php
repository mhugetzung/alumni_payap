<?php
require_once("../../pages/auth.inc.php");

if(isset($_GET['fac_id'])) {
    $fac_id = $_GET['fac_id'];

    $sql = "DELETE FROM faculty WHERE fac_id = '$fac_id'";
    $result = mysqli_query($conn, $sql) or die("Error in query: $sql " . mysqli_error($conn));

    echo "<script>";
    echo "alert('ลบข้อมูลเรียบร้อย')";
    echo "</script>";

    header("Refresh:0; url=../../pages/faculty/index.php");
} else {
    header("Location: ../../pages/faculty/index.php");
}
mysqli_close($conn);