<?php
require_once("../../pages/auth.inc.php");

if(isset($_GET['admin_id']) && isset($_POST)){
    $admin_id = $_GET['admin_id'];
    $admin_no = $_POST["admin_no"] ?? '';
    $admin_firstname = $_POST["admin_firstname"] ?? '';
    $admin_lastname = $_POST["admin_lastname"] ?? '';
    $admin_position = $_POST["admin_position"] ?? '';
    $admin_email = $_POST["admin_email"] ?? '';
    $admin_phone = $_POST["admin_phone"] ?? '';
    

    $sql = "UPDATE admin 
            SET 
                admin_no = '$admin_no',
                admin_firstname = '$admin_firstname',
                admin_lastname = '$admin_lastname',
                admin_position = '$admin_position',
                admin_email = '$admin_email',
                admin_phone = '$admin_phone'
            WHERE admin_id = '$admin_id'";
    $result = mysqli_query($conn, $sql) or die("Error in query: $sql " . mysqli_error($conn));

    echo "<script>";
    echo "alert('แก้ไขข้อมูลเรียบร้อย')";
    echo "</script>";

    header("Refresh:0; url=../../pages/dashboard/index.php");
} else {
    header("Location: ../../pages/admin/edit.php");
}
mysqli_close($conn);