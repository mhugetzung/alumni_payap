<?php
require_once("../../pages/auth.inc.php");

if(isset($_GET['admin_id'])) {
    $admin_id = $_GET['admin_id'];

    $sql = "DELETE FROM admin WHERE admin_id = '$admin_id'";
    $result = mysqli_query($conn, $sql) or die("Error in query: $sql " . mysqli_error($conn));

    echo "<script>";
    echo "alert('ลบข้อมูลเรียบร้อย')";
    echo "</script>";

    header("Refresh:0; url=../../pages/admin/index.php");
} else {
    header("Location: ../../pages/admin/index.php");
}
mysqli_close($conn);