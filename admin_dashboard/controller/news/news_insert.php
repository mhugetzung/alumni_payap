<?php
require_once("../../pages/auth.inc.php");

if(isset($_POST)) {
    $news_title = $_POST["news_title"];
    $news_detail = $_POST["news_detail"];
    $news_title_en = $_POST["news_title_en"];
    $news_detail_en = $_POST["news_detail_en"];
    $news_created_at = date('y-m-d H:i:s');
    $admin_id = $_SESSION["admin_id"];
    $news_img = $_FILES["news_img"]['name'];
    $img_tmp = $_FILES['news_img']['tmp_name'];

    move_uploaded_file($img_tmp, "../../../images/news/$news_img");

    $sql = "INSERT INTO news (news_title, news_detail, news_title_en, news_detail_en, news_created_at, news_img, admin_id ) 
                VALUES ('$news_title', '$news_detail', '$news_title_en', '$news_detail_en', '$news_created_at', '$news_img', '$admin_id')";
    $result = mysqli_query($conn, $sql) or die("Error in query: $sql " . mysqli_error($conn));

    echo "<script>";
    echo "alert('เพิ่มข้อมูลเรียบร้อย')";
    echo "</script>";

    header("Refresh:0; url=../../pages/news/index.php");
} else {
    header("Location: ../../pages/news/create.php");
}
mysqli_close($conn);