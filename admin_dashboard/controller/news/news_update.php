<?php
require_once("../../pages/auth.inc.php");

if(isset($_GET['news_id']) && isset($_POST)) {
    $news_id = $_GET['news_id'];
    $news_title = $_POST["news_title"];
    $news_detail = $_POST["news_detail"];
    $news_title_en = $_POST["news_title_en"];
    $news_detail_en = $_POST["news_detail_en"];
    $news_created_at = date('y-m-d H:i:s');
    $news_img = $_FILES["news_img"]['name'];
    $img_tmp = $_FILES['news_img']['tmp_name'];

    move_uploaded_file($img_tmp, "../../../images/news/$news_img");

    $sql = "UPDATE news
            SET news_title = '$news_title',
                news_detail = '$news_detail',
                news_title_en = '$news_title_en',
                news_detail_en = '$news_detail_en',
                news_img = '$news_img',
                news_created_at = '$news_created_at'
                
            WHERE news_id = '$news_id'";
    $result = mysqli_query($conn, $sql) or die("Error in query: $sql " . mysqli_error($conn));

    echo "<script>";
    echo "alert('แก้ไขข้อมูลเรียบร้อย')";
    echo "</script>";

    header("Refresh:0; url=../../pages/news/index.php?news_id=$news_id");
} else {
    header("Location: ../../pages/news/edit.php");
}
mysqli_close($conn);