<?php
require_once("../../pages/auth.inc.php");

if(isset($_GET['alumni_id'])) {
    $alumni_id = $_GET['alumni_id'];
    $status = $_GET['checked'];

    $sql = "UPDATE alumni SET status = $status WHERE alumni_id = '$alumni_id'";
    $result = mysqli_query($conn, $sql) or die("Error in query: $sql " . mysqli_error($conn));

    echo json_encode([
        "result" => true
    ]);
} else {
    header("Location: ../../pages/user/index.php");
}
mysqli_close($conn);