<?php
require_once("../../pages/auth.inc.php");

if(isset($_GET['alumni_id'])) {
    $alumni_id = $_GET['alumni_id'];

    $sql = "DELETE FROM alumni WHERE alumni_id = '$alumni_id'";
    $result = mysqli_query($conn, $sql) or die("Error in query: $sql " . mysqli_error($conn));

    echo "<script>";
    echo "alert('ลบข้อมูลเรียบร้อย')";
    echo "</script>";

    header("Refresh:0; url=../../pages/user/index.php");
} else {
    header("Location: ../../pages/user/index.php");
}
mysqli_close($conn);