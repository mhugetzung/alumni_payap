<?php
require_once("../../pages/auth.inc.php");
$sql1 = "SELECT * FROM alumni";
$result1 = mysqli_query($conn, $sql1);
$row = mysqli_fetch_array($result1);
$alumni_id = $row["alumni_id"];

if (isset($_GET['alumni_id']) && isset($_POST)) {
    $alumni_id = $_GET['alumni_id'];
    $fac_id = $_POST["fac_id"] ?? '';
    $dep_id = $_POST["dep_id"] ?? '';
    $pro_id = $_POST["pro_id"] ?? '';

    $sqlup = "UPDATE alumni
            SET  fac_id = '$fac_id',
                 dep_id = '$dep_id',
                 pro_id = '$pro_id'

            WHERE alumni_id = '$alumni_id'";
    $result = mysqli_query($conn, $sqlup) or die("Error in query: $sqlup " . mysqli_error($conn));

    echo "<script>";
    echo "alert('แก้ไขข้อมูลเรียบร้อย')";
    echo "</script>";

    header("Refresh:0; url=../../../pages/alumni/detail.php?alumni_id=$alumni_id");
} else {

    header("Location: ../../pages/user/edit.php?alumni_id=$alumni_id");
}
mysqli_close($conn);
