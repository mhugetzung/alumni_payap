<?php
require_once("../../pages/auth.inc.php");



if (isset($_POST['importSubmit'])) {

    // Allowed mime types
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');

    // Validate whether selected file is a CSV file
    if (!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)) {

        // If the file is uploaded
        if (is_uploaded_file($_FILES['file']['tmp_name'])) {

            // Open uploaded CSV file with read-only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');

            // Skip the first line
            fgetcsv($csvFile);

            // Parse data from CSV file line by line
            while (($line = fgetcsv($csvFile)) !== FALSE) {
                // Get row data
                $alumni_stuid   = $line[0];
                $alumni_fname  = $line[1];
                $alumni_firstname  = $line[2];
                $alumni_lastname  = $line[3];
                $alumni_email = $line[4];
                $alumni_status = $line[5];
                $fac_name = $line[6];
                $dep_name = $line[7];
                $pro_name = $line[8];
                $modified = date('y-m-d H:i:s');
                $created = date('y-m-d H:i:s');

                // Check whether member already exists in the database with the same email
                $prevQuery = "SELECT alumni_id FROM alumni WHERE alumni_stuid = '" . $line[0] . "'";
                $prevResult = $conn->query($prevQuery);

                $sqlDep = "SELECT dep_id FROM department WHERE dep_name = '$dep_name'";
                $queryDep = $conn->query($sqlDep) or die("Error in query: $sqlDep " . mysqli_error($conn));
                $getDep = $queryDep->fetch_assoc();
                $dep_id = $getDep['dep_id'] ?? '';

                $sqlfac = "SELECT fac_id FROM faculty WHERE fac_name = '$fac_name'";
                $queryfac = $conn->query($sqlfac) or die("Error in query: $sqlfac " . mysqli_error($conn));
                $getfac = $queryfac->fetch_assoc();
                $fac_id = $getfac['fac_id'] ?? '';

                $sqlpro = "SELECT pro_id FROM program WHERE pro_name = '$pro_name'";
                $querypro = $conn->query($sqlDep) or die("Error in query: $sqlpro " . mysqli_error($conn));
                $getpro = $querypro->fetch_assoc();
                $pro_id = $getpro['pro_id'] ?? '';

                if ($prevResult->num_rows > 0) {
                    // Update member data in the database
                    $sqlup = "UPDATE alumni
                      SET alumni_stuid = '" . $alumni_stuid . "', alumni_fname = '" . $alumni_fname . "',
                      alumni_firstname = '" . $alumni_firstname . "', alumni_lastname = '" . $alumni_lastname . "',
                      alumni_email = '" . $alumni_email . "', alumni_status = '" . $alumni_status . "', fac_id = '" . $fac_id . "',
                      dep_id = '" . $dep_id . "', pro_id = '" . $pro_id . "', modified = '" . $modified . "'
                      WHERE alumni_stuid = '" . $alumni_stuid . "'";

                    $result1 = mysqli_query($conn, $sqlup) or die("Error in query: $sqlup " . mysqli_error($conn));
                } else {
                    // Insert member data in the database

                    $sqlin = "INSERT INTO alumni (alumni_stuid, alumni_fname, alumni_firstname, alumni_lastname,
                       alumni_email, alumni_status, fac_id, dep_id, pro_id, created, modified) VALUES ('" . $alumni_stuid . "',
                        '" . $alumni_fname . "', '" . $alumni_firstname . "', '" . $alumni_lastname . "', '" . $alumni_email . "', 
                        '" . $alumni_status . "', '" . $fac_id . "', '" . $dep_id . "', '" . $pro_id . "', '" . $modified . "', '" . $created . "')";
                    $result2 = mysqli_query($conn, $sqlin) or die("Error in query: $sqlin " . mysqli_error($conn));
                }
            }

            // Close opened CSV file
            fclose($csvFile);

            $qstring = '?status=succ';
        } else {
            $qstring = '?status=err';
        }
    } else {
        $qstring = '?status=invalid_file';
    }
}

// Redirect to the listing page
header("Location: ../../pages/user/index.php" . $qstring);
