$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sProcessing": "กำลังดำเนินการ...",
        "sLengthMenu": "แสดง_MENU_ รายการ",
        "sZeroRecords": "ไม่พบข้อมูล",
        "sInfo": "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 รายการจาก 0 รายการ",
        "sInfoFiltered": "(ข้อมูลทั้งหมด _MAX_ รายการ)",
        "sInfoPostFix": "",
        "sSearch": "ค้นหา:",
        "sUrl": "",
        "oPaginate": {
            "sFirst": "เิริ่มต้น",
            "sPrevious": "ก่อนหน้า",
            "sNext": "ถัดไป",
            "sLast": "สุดท้าย"
        }
    }
});
$(document).ready(function() {
    $('#dataTable').DataTable();
});