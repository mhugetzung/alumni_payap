<?php
require_once("../auth.inc.php");
$alumni_id = $_GET['alumni_id'];

$query = "SELECT * FROM alumni WHERE alumni_id = '$alumni_id'";
$result = mysqli_query($conn, $query) or die("Error in query: $sql " . mysqli_error($conn));
$alumni = mysqli_fetch_assoc($result);

$query_fac = "SELECT * FROM faculty";
$result_fac = mysqli_query($conn, $query_fac) or die("Error in query: $sql " . mysqli_error($conn));

$query_dep = "SELECT * FROM department";
$result_dep = mysqli_query($conn, $query_dep) or die("Error in query: $sql " . mysqli_error($conn));

$query_pro = "SELECT * FROM program";
$result_pro = mysqli_query($conn, $query_pro) or die("Error in query: $sql " . mysqli_error($conn));
?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/logo.png">
    <link rel="icon" type="image/png" href="../../assets/img/logo.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>แก้ไขหลักสูตร/สาขา/คณะ | มหาวิทยาลัยพายัพ</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <link href="../../assets/css/demo.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Mitr&display=swap" rel="stylesheet">
    <style>
        .a-text {
            color: #3472F7;
        }

        .text {
            color: black;
        }

        .btn-primary {
            border-color: #3472F7;
            color: #3472F7;
        }

        .btn {
            border-width: 2px;
            background-color: transparent;
            font-weight: 400;
            opacity: 0.8;
            filter: alpha(opacity=80);
            padding: 8px 16px;
            border-color: #3472F7;
            color: #3472F7;
        }

        .p-text {
            color: white;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <?php include_once("../../components/sidebar.inc.php") ?>
        <div class="main-panel">
            <?php include_once("../../components/navbar.inc.php") ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <h4 class="card-title">แก้ไขหลักสูตร/สาขา/คณะ</h4>
                        </div>
                        <?php
                        if (isset($_GET['alumni_id'])) {
                            $query = "SELECT * FROM alumni
                            LEFT JOIN faculty
                            ON alumni.fac_id = faculty.fac_id
                            LEFT JOIN program
                            ON alumni.pro_id = program.pro_id
                            LEFT JOIN department
                            ON alumni.dep_id = department.dep_id
                            WHERE alumni.alumni_id = '$alumni_id'";
                            $result = mysqli_query($conn, $query);
                            while ($row = mysqli_fetch_array($result)) {
                                $alumni_stuid = $row['alumni_stuid'];
                                $alumni_fname = $row['alumni_fname'];
                                $alumni_firstname = $row['alumni_firstname'];
                                $alumni_lastname = $row['alumni_lastname'];
                                $dep_id = $row['dep_id'];
                                $fac_id = $row['fac_id'];
                                $pro_id = $row['pro_id'];
                        ?>
                                <div class="card-body">
                                    <form action="../../controller/user/user_update.php?alumni_id=<?= $alumni['alumni_id'] ?>" method="post" enctype="multipart/form-data">
                                        <center>
                                            <div class="form-group col-md-6 mt-4">
                                                <label class="text">รหัสนักศึกษา - ชื่อนามสกุล</label>
                                                <input type="text" name="alumni_stuid" class="form-control" value=" <?= $alumni_stuid ?> <?= $alumni_fname ?> <?= $alumni_firstname ?> <?= $alumni_lastname ?>" required disabled>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="text">จบจากสาขาวิชา</label>
                                                <select class="form-control" name="dep_id" aria-label="Default select example" required>
                                                    <option selected>เลือกสาขาวิชา</option>
                                                    <?php while ($row = mysqli_fetch_assoc($result_dep)) { ?>
                                                        <option required value="<?= $row['dep_id'] ?>" <?= $row['dep_id'] == $alumni['dep_id'] ? 'selected' : '' ?>><?= $row['dep_name'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="text">จบจากคณะวิชา</label>
                                                <select class="form-control" name="fac_id" aria-label="Default select example" required>
                                                    <option selected>เลือกคณะวิชา</option>
                                                    <?php while ($row = mysqli_fetch_assoc($result_fac)) { ?>
                                                        <option required value="<?= $row['fac_id'] ?>" <?= $row['fac_id'] == $alumni['fac_id'] ? 'selected' : '' ?>><?= $row['fac_name'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="text">จบจากหลักสูตร</label>
                                                <select class="form-control" name="pro_id" aria-label="Default select example" required>
                                                    <option selected>เลือกหลักสูตร</option>
                                                    <?php while ($row = mysqli_fetch_assoc($result_pro)) { ?>
                                                        <option required value="<?= $row['pro_id'] ?>" <?= $row['pro_id'] == $alumni['pro_id'] ? 'selected' : '' ?>><?= $row['pro_name_th'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6 mb-4">

                                                <button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
                                                <a class="btn btn-primary" href="index.php" role="button">ยกเลิก</a>
                                            </div>
                                        </center>
                                    </form>
                                </div>
                        <?php }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php include_once("../../components/footer.inc.php") ?>
        </div>
    </div>

    <script src="../../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../assets/js/plugins/bootstrap-switch.js"></script>
    <script src="../../assets/js/plugins/bootstrap-notify.js"></script>
    <script src="../../assets/js/light-bootstrap-dashboard.js?v=2.0.0 " type="text/javascript"></script>
    <script src="../../assets/js/demo.js"></script>
</body>

</html>