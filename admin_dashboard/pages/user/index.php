<?php
require_once("../auth.inc.php");
if (!empty($_GET['status'])) {
    switch ($_GET['status']) {
        case 'succ':
            $statusType = 'alert-success';
            $statusMsg = 'เพิ่มข้อมูลศิษย์เก่าสำเร็จ';
            break;
        case 'err':
            $statusType = 'alert-danger';
            $statusMsg = 'เกิดปัญหาขึ้น! โปรดลองอีกครั้ง';
            break;
        case 'invalid_file':
            $statusType = 'alert-danger';
            $statusMsg = 'อัพโหลดด้วยไฟล์ .csv';
            break;
        default:
            $statusType = '';
            $statusMsg = '';
    }
}
?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/logo.png">
    <link rel="icon" type="image/png" href="../../assets/img/logo.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>เพิ่มข้อมูลศิษย์เก่า| มหาวิทยาลัยพายัพ</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <link href="../../assets/css/demo.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Mitr&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.11.2/css/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <?php include_once("../../components/sidebar.inc.php") ?>
        <div class="main-panel">
            <?php include_once("../../components/navbar.inc.php") ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between ">
                            <h4 class="card-title">จัดการศิษย์เก่า</h4>
                            <?php if (!empty($statusMsg)) { ?>
                                <div class="col-xs-12">
                                    <div class="alert <?php echo $statusType; ?>"><?php echo $statusMsg; ?></div>
                                </div>
                            <?php } ?>
                            <a href="javascript:void(0);" class="btn btn-primary mb-3" onclick="formToggle('importFrm');"><i class="plus"></i>เพิ่มศิษย์เก่า</a>
                        </div>

                        <!-- Import link -->
                        <!-- CSV file upload form -->
                        <div class="col-md-12" id="importFrm" style="display: none;">
                            <form action="../../controller/user/importData.php" method="post" enctype="multipart/form-data">
                                <center><input type="file" name="file" accept=".csv" />
                                    <input type="submit" class="btn btn-primary mb-3" name="importSubmit" value="นำเข้า">
                                </center>
                            </form>
                        </div>
                        <!-- Data list table -->
                        <?php
                        $query = "SELECT * FROM alumni ORDER BY alumni_id asc";
                        $result = mysqli_query($conn, $query);
                        $arr_alumni = [];
                        $i = 1;
                        if ($result->num_rows > 0) {
                            $arr_alumni = $result->fetch_all(MYSQLI_ASSOC);
                        }
                        ?>
                        <div class="card-body table-full-width table-responsive">
                            <table class="table table-hover table-striped" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>รหัสนักศึกษา</th>
                                        <th>คำนำหน้าชื่อ</th>
                                        <th>ชื่อ</th>
                                        <th>นามสกุล</th>
                                        <th>อีเมล</th>
                                        <th>แสดงหน้าแรก</th>
                                        <th><center>ตัวเลือก</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($arr_alumni)) { ?>
                                        <?php foreach ($arr_alumni as $alumni) { ?>
                                            <tr>
                                                <td><?php echo $i ?></td>
                                                <td><?php echo $alumni['alumni_stuid']; ?></td>
                                                <td><?php echo $alumni['alumni_fname']; ?></td>
                                                <td><?php echo $alumni['alumni_firstname']; ?></td>
                                                <td><?php echo $alumni['alumni_lastname']; ?></td>
                                                <td><?php echo $alumni['alumni_email']; ?></td>
                                                <td>
                                                    <div class="onoffswitch">
                                                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox toggle-show-homepage" data-id="<?= $alumni['alumni_id'] ?>" id="myonoffswitch<?= $alumni['alumni_stuid'] ?>" tabindex="0"  <?= $alumni['status'] ? 'checked' : '' ?>>
                                                        <label class="onoffswitch-label" for="myonoffswitch<?= $alumni['alumni_stuid'] ?>">
                                                            <span class="onoffswitch-inner"></span>
                                                            <span class="onoffswitch-switch"></span>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <a href="../../../pages/alumni/edit.php?alumni_id=<?= $alumni['alumni_id'] ?>" target="_blank" class="text-primary">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="edit.php?alumni_id=<?= $alumni['alumni_id'] ?>" class="text-primary">
                                                        <i class="fa fa-book" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="../../../pages/alumni/detail.php?alumni_id=<?= $alumni['alumni_id'] ?>" target="_blank" class="text-primary">
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </a>
                                                    <?php if ($_SESSION["admin_role_id"] == 1) { ?>
                                                        <a href="../../controller/user/user_delete.php?alumni_id=<?= $alumni['alumni_id'] ?>" onclick="return confirm('โปรดยืนยันเพื่อลบข้อมูล')" class="text-primary">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                        </a>
                                                    </td>
                                                    <?php } ?>
                                            </tr>
                                        <?php $i++;
                                        } ?>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <?php include_once("../../components/footer.inc.php") ?>
        </div>
    </div>

    <script src="../../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../assets/js/plugins/bootstrap-switch.js"></script>
    <script src="../../assets/js/plugins/bootstrap-notify.js"></script>
    <script src="../../assets/js/light-bootstrap-dashboard.js?v=2.0.0 " type="text/javascript"></script>
    <script src="../../assets/js/demo.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.2/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.2/js/dataTables.bootstrap4.min.js"></script>
    <script src="../../assets/script.js"></script>
    <script>
        $(".toggle-show-homepage").change(async function() {
            const isChecked = $(this).is(':checked')
            const alumni_id = $(this).data('id');

            const res = await fetch(`../../controller/user/update_show.php?alumni_id=${alumni_id}&checked=${isChecked}`)
            const status = await res.json()
            
        });
    </script>
    <script>
        function formToggle(ID) {
            var element = document.getElementById(ID);
            if (element.style.display === "none") {
                element.style.display = "block";
            } else {
                element.style.display = "none";
            }
        }
    </script>
</body>

</html>