<?php
require_once("../auth.inc.php");

if(isset($_GET['pro_id'])) {
    $pro_id = $_GET['pro_id'];
    
    $query = "SELECT * FROM program WHERE pro_id = '$pro_id'";
    $result = mysqli_query($conn, $query) or die("Error in query: $sql " . mysqli_error($conn));
    $pro = mysqli_fetch_assoc($result);


    $query_fac = "SELECT * FROM faculty";
    $result_fac = mysqli_query($conn, $query_fac) or die("Error in query: $sql " . mysqli_error($conn));
    // print_r($pro);
    // return;
} else {
    header("Location: index.php");
}
?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/logo.png">
    <link rel="icon" type="image/png" href="../../assets/img/logo.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>แก้ไขข้อมูลหลักสูตร | มหาวิทยาลัยพายัพ</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <link href="../../assets/css/demo.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Mitr&display=swap" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <?php include_once("../../components/sidebar.inc.php") ?>
        <div class="main-panel">
            <?php include_once("../../components/navbar.inc.php") ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="card">
                    <div class="card-header d-flex justify-content-between">
                                    <h4 class="card-title">แก้ไขข้อมูลหลักสูตร</h4>
                                </div>
                        <div class="card-body">
                            <form action="../../controller/program/program_update.php?pro_id=<?= $pro['pro_id'] ?>" method="post">
                                <div class="col">
                                <div class="form-group">
                                        <label style=color:red;>* </label>&nbsp;<label>ชื่อหลักสูตรภาษาไทย</label>
                                        <input type="text" class="form-control" name="pro_name_th" placeholder="ชื่อหลักสูตรภาษาไทย" onkeypress="return " required maxlength="100" value="<?= $pro['pro_name_th'] ?>">
                                    </div>
                                    <div class="form-group">
                                        &nbsp;<label>ชื่อหลักสูตรภาษาอังกฤษ</label>
                                        <input type="text" class="form-control" name="pro_name_eng" placeholder="ชื่อหลักสูตรภาษาอังกฤษ" onkeypress="return " maxlength="100" value="<?= $pro['pro_name_eng'] ?>">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label style=color:red;>* </label>&nbsp;<label>เลือกคณะวิชา</label>
                                        <select class="form-control" name="fac_id" required>
                                        <option value=''>เลือกคณะ</option>
                                        <?php while($row = mysqli_fetch_assoc($result_fac)) {?>
                                            <option value="<?= $row['fac_id'] ?>" <?= $row['fac_id'] == $pro['fac_id'] ? 'selected' : '' ?>><?= $row['fac_name'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label style=color:red;>* </label>&nbsp;<label>เลือกหลักสูตร</label>
                                        <select class="form-control" name="pro_level" required>
                                            <option value="">เลือกหลักสูตร</option>
                                            <option value="หลักสูตรปริญญาตรี (หลักสูตรไทย)"<?= $pro['pro_level'] == "หลักสูตรปริญญาตรี (หลักสูตรไทย)" ? 'selected' : '' ?>>หลักสูตรปริญญาตรี (หลักสูตรไทย)</option>
                                            <option value="หลักสูตรปริญญาโท (หลักสูตรไทย)"<?= $pro['pro_level'] == "หลักสูตรปริญญาโท (หลักสูตรไทย)" ? 'selected' : '' ?>>หลักสูตรปริญญาโท (หลักสูตรไทย)</option>
                                            <option value="หลักสูตรปริญญาเอก (หลักสูตรนานาชาติ)"<?= $pro['pro_level'] == "หลักสูตรปริญญาเอก (หลักสูตรนานาชาติ)" ? 'selected' : '' ?>>หลักสูตรปริญญาเอก (หลักสูตรนานาชาติ)</option>
                                            <option value="หลักสูตระยะสั้นและประกาศนียบัตร"<?= $pro['pro_level'] == "หลักสูตระยะสั้นและประกาศนียบัตร" ? 'selected' : '' ?>>หลักสูตระยะสั้นและประกาศนียบัตร</option>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="col">
                                    <button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
                                    <a class="btn btn-primary" href="index.php" role="button">ยกเลิก</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php include_once("../../components/footer.inc.php") ?>
        </div>
    </div>

    <script src="../../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../assets/js/plugins/bootstrap-switch.js"></script>
    <script src="../../assets/js/plugins/bootstrap-notify.js"></script>
    <script src="../../assets/js/light-bootstrap-dashboard.js?v=2.0.0 " type="text/javascript"></script>
    <script src="../../assets/js/demo.js"></script>
</body>

</html>