<?php
session_start();

if (isset($_SESSION['admin_id'])) {
    header("Location: ../dashboard/index.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>เข้าสู่ระบบสำหรับบุคลากร | ศิษย์เก่ามหาวิทยาลัยพายัพ</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../../assets/img/logo.png" />
    <link rel="stylesheet" type="text/css" href="../../../login/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../login/vendor/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="../../../login/vendor/css-hamburgers/hamburgers.min.css">
    <link rel="stylesheet" type="text/css" href="../../../login/vendor/animsition/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="../../../login/vendor/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="../../../login/vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="../../../login/css/util.css">
    <link rel="stylesheet" type="text/css" href="../../../login/css/main.css">
    <!-- <link href="https://fonts.googleapis.com/css2?family=Mitr&display=swap" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css2?family=Prompt:wght@300;400;500;700;800;900&display=swap" rel="stylesheet">
</head>

<body>

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form action="../../controller/auth/login.php" method="POST" class="login100-form validate-form p-l-55 p-r-55 p-t-178">
                    <span class="login100-form-title">
                    <img src="../../../images/logo-login.png" width="90" height="90" alt="logo">
                    </span>
                    <br>
                    <span class="txt1">
                        ชื่อผู้ใช้งาน หรือ รหัสบุคลากร
                    </span>
                    <div class="wrap-input100 validate-input m-b-16" data-validate="กรุณาใส่ชื่อผู้ใช้">
                        <input class="input100" type="text" name="admin_username" placeholder="">
                        <span class="focus-input100"></span>
                    </div>
                    <span class="txt1">
                        รหัสผ่าน
                    </span>
                    <div class="wrap-input100 validate-input" data-validate="กรุณาใส่รหัสผ่าน">
                        <input class="input100" type="password" name="admin_password" placeholder="">
                        <span class="focus-input100"></span>
                    </div>

                    <div class="flex-col-c p-t-40 p-b-20">
                        <div class="container-login100-form-btn p-b-10">
                            <input type="submit" name="submit_login" class="login100-form-btn" value="เข้าสู่ระบบ">
                        </div>
                        <div class="container-login200-form-btn">
                            <a href="../../../index.php">
                                ย้อนกลับ
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script src="../../../login/vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="../../../login/vendor/animsition/js/animsition.min.js"></script>
    <script src="../../../login/vendor/bootstrap/js/popper.js"></script>
    <script src="../../../login/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../login/vendor/select2/select2.min.js"></script>
    <script src="../../../login/vendor/daterangepicker/moment.min.js"></script>
    <script src="../../../login/vendor/daterangepicker/daterangepicker.js"></script>
    <script src="../../../login/vendor/countdowntime/countdowntime.js"></script>
    <script src="../../../login/js/main.js"></script>
</body>

</html>