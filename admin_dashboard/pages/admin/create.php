<?php
require_once("../auth.inc.php");
?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/logo.png">
    <link rel="icon" type="image/png" href="../../assets/img/logo.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>เพิ่มข้อมูลผู้ดูแล | มหาวิทยาลัยพายัพ</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <link href="../../assets/css/demo.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Mitr&display=swap" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <?php include_once("../../components/sidebar.inc.php") ?>
        <div class="main-panel">
            <?php include_once("../../components/navbar.inc.php") ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <h4 class="card-title">เพิ่มผู้ดูแล</h4>
                        </div>
                        <div class="card-body">
                            <form action="../../controller/admin/admin_insert.php" method="post">
                                <div class="col">
                                    <div class="form-group">
                                        <label style=color:red;>* </label>&nbsp;<label>ชื่อผู้ใช้งาน</label>
                                        <input type="text" class="form-control" name="admin_username" placeholder="ชื่อผู้ใช้" onkeypress="return bannedKeyEng(event)" required maxlength="15">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label style=color:red;>* </label>&nbsp;<label>รหัสผ่าน</label>
                                        <input type="password" class="form-control" name="admin_password" placeholder="รหัสผ่าน" onkeypress="return bannedKeyEngandNum(event)" required maxlength="15">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label style=color:red;>* </label>&nbsp;<label>เลือกประเภทของผู้ใช้งาน</label>
                                        <select class="form-control" name="admin_role_id2" required>
                                            <?php if (!(isset($_GET["admin_role_name"]))) {
                                                echo "<option value=''>เลือกประเภทของผู้ใช้งาน</option>";
                                            } ?>
                                            <?php
                                            $role_result = mysqli_query($conn, "SELECT * FROM role");
                                            while ($role_row = mysqli_fetch_assoc($role_result)) {
                                                if ($_GET["admin_role_name"] == $role_row["admin_role_id"]) {
                                                    echo "<option value=" . $role_row["admin_role_id"] . " selected>" . $role_row["admin_role_name"] . "</option>";
                                                } else {
                                                    echo "<option value=" . $role_row["admin_role_id"] . ">" . $role_row["admin_role_name"] . "</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-4">
                                        <label style=color:red;>* </label>&nbsp;<label>รหัสบุคลากร</label>
                                        <input type="text" class="form-control" name="admin_no" placeholder="รหัสบุคลากร" " maxlength=" 10" required>
                                    </div>
                                    <div class="col-4">
                                        <label style=color:red;>* </label>&nbsp;<label>ชื่อ</label>
                                        <input type="text" class="form-control" name="admin_firstname" placeholder="ชื่อ" onkeypress="return bannedKeyEng(event)" required>
                                    </div>
                                    <div class="col-4">
                                        <label style=color:red;>* </label>&nbsp;<label>นามสกุล</label>
                                        <input type="text" class="form-control" name="admin_lastname" placeholder="นามสกุล" onkeypress="return bannedKeyEng(event)" required>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>ตำแหน่ง</label>
                                        <input type="text" class="form-control" name="admin_position" placeholder="ตำแหน่งงาน" onkeypress="return bannedKeyEng(event)" maxlength="30">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>อีเมล</label>
                                        <input type="text" class="form-control" name="admin_email" placeholder="อีเมล" onkeypress="return bannedKeyEngandNum(event)" maxlength="50">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>เบอร์โทรศัพท์</label>
                                        <input type="text" class="form-control" name="admin_phone" placeholder="เบอร์โทรศัพท์ / เบอร์โทรภายใน" onkeypress="return bannedKeyEng(event)" maxlength="15">
                                    </div>
                                </div>
                                <br>
                                <div class="col">
                                    <button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
                                    <a class="btn btn-primary" href="index.php" role="button">ยกเลิก</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php include_once("../../components/footer.inc.php") ?>
        </div>
    </div>

    <script src="../../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../assets/js/plugins/bootstrap-switch.js"></script>
    <script src="../../assets/js/plugins/bootstrap-notify.js"></script>
    <script src="../../assets/js/light-bootstrap-dashboard.js?v=2.0.0 " type="text/javascript"></script>
    <script src="../../assets/js/demo.js"></script>
</body>

</html>