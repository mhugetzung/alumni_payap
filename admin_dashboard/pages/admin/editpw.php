<?php
require_once("../auth.inc.php");
$admin_id = $_GET['admin_id'];
if(!isset($_SESSION['admin_id']) && $admin_id != $_SESSION['admin_id']) {
    header("Location: ../dashboard/index.php");
}
?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/logo.png">
    <link rel="icon" type="image/png" href="../../assets/img/logo.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>เปลี่ยนรหัสผ่าน | มหาวิทยาลัยพายัพ</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <link href="../../assets/css/demo.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Mitr&display=swap" rel="stylesheet">
    <style>
        .a-text {
            color: #3472F7;
        }

        .text {
            color: black;
        }

        .btn-primary {
            border-color: #3472F7;
            color: #3472F7;
        }

        .btn {
            border-width: 2px;
            background-color: transparent;
            font-weight: 400;
            opacity: 0.8;
            filter: alpha(opacity=80);
            padding: 8px 16px;
            border-color: #3472F7;
            color: #3472F7;
        }

        .p-text {
            color: white;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <?php include_once("../../components/sidebar.inc.php") ?>
        <div class="main-panel">
            <?php include_once("../../components/navbar.inc.php") ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <h4 class="card-title">เปลี่ยนรหัสผ่าน</h4>
                        </div>
                        <?php
                        if (isset($_GET['admin_id'])) {
                            $admin_id = $_GET['admin_id'];
                            $query = "SELECT *
                            FROM admin WHERE admin_id = '$admin_id'";
                            $result = mysqli_query($conn, $query);
                            while ($row = mysqli_fetch_array($result)) {
                                $admin_id = $row['admin_id'];
                                $admin_username = $row['admin_username'];

                        ?>
                                <div class="card-body">
                                    <form action="../../controller/admin/changepassword.php" method="post">
                                        <center>
                                            <div class="form-group col-md-6 mt-4">
                                                <label class="text mb-2">ชื่อผู้ใช้งาน</label>
                                                <input type="text" name="admin_username" class="form-control" value=" <?= $admin_username ?>" required disabled>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="text mb-2">รหัสผ่านใหม่</label>
                                                <input type="password" name="np" class="form-control" placeholder="กรอกรหัสผ่านใหม่" required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="text mb-2">ยืนยันรหัสผ่านใหม่</label>
                                                <input type="password" name="c_np" class="form-control" placeholder="กรอกยืนยันรหัสผ่านใหม่" required>
                                            </div>
                                            <div class="form-group col-md-6 mb-4">
                                                <input type="hidden" name="alumni_id" class="form-control" value=" <?= $alumni_id ?>">
                                                <button type="submit" class="btn btn-success">เปลี่ยนรหัสผ่าน</button>
                                                <a class="btn btn-primary" href="index.php" role="button">ยกเลิก</a>
                                            </div>
                                        </center>
                                    </form>
                                </div>
                        <?php }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php include_once("../../components/footer.inc.php") ?>
        </div>
    </div>

    <script src="../../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../assets/js/plugins/bootstrap-switch.js"></script>
    <script src="../../assets/js/plugins/bootstrap-notify.js"></script>
    <script src="../../assets/js/light-bootstrap-dashboard.js?v=2.0.0 " type="text/javascript"></script>
    <script src="../../assets/js/demo.js"></script>
</body>

</html>