<?php
require_once("../auth.inc.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/logo.png">
    <link rel="icon" type="image/png" href="../../assets/img/logo.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>ข้อมูลศิษย์เก่าพายัพ | มหาวิทยาลัยพายัพ</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <link href="../../assets/css/demo.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Mitr&display=swap" rel="stylesheet">
    <link href="/your-path-to-fontawesome/css/fontawesome.css" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <?php
        include_once("../../components/sidebar.inc.php") ?>
        <div class="main-panel">
            <?php include_once("../../components/navbar.inc.php") ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <?php
                        $queryalumni = "SELECT COUNT(*) FROM alumni";
                        $resultalumni = mysqli_query($conn, $queryalumni) or die("Error in sql : $queryalumni" .
                            mysqli_error($conn));
                        $row = mysqli_fetch_row($resultalumni);
                        $alumni = $row[0];
                        ?>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header">
                                    <p class="card-category  text-center">ศิษย์เก่า</p>
                                    <h4 class="card-title text-center  mb-4"><?= $alumni; ?> <i class="fa fa-graduation-cap fa-lg" style="color:#4581E0"></i></h4>
                                </div>
                            </div>
                        </div>
                        <?php
                        $querydep = "SELECT COUNT(*) FROM department";
                        $resultdep = mysqli_query($conn, $querydep) or die("Error in sql : $querydep" .
                            mysqli_error($conn));
                        $row = mysqli_fetch_row($resultdep);
                        $dep = $row[0];
                        ?>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header ">
                                    <p class="card-category  text-center">สาขาวิชา</p>
                                    <h4 class="card-title text-center  mb-4"><?= $dep; ?> <i class="fa fa-inbox fa-lg" style="color:#4581E0"></i></h4>
                                </div>
                            </div>
                        </div>
                        <?php
                        $queryfac = "SELECT COUNT(*) FROM faculty";
                        $resultfac = mysqli_query($conn, $queryfac) or die("Error in sql : $queryfac" .
                            mysqli_error($conn));
                        $row = mysqli_fetch_row($resultfac);
                        $fac = $row[0];
                        ?>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header ">
                                    <p class="card-category  text-center">คณะวิชา</p>
                                    <h4 class="card-title text-center  mb-4"><?= $fac; ?> <i class="fa fa-inbox fa-lg" style="color:#4581E0"></i></h4>
                                </div>
                            </div>
                        </div>

                        <?php
                        $querypro = "SELECT COUNT(*) FROM program";
                        $resultpro = mysqli_query($conn, $querypro) or die("Error in sql : $querypro" .
                            mysqli_error($conn));
                        $row = mysqli_fetch_row($resultpro);
                        $pro = $row[0];
                        ?>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header ">
                                    <p class="card-category  text-center">หลักสูตร</p>
                                    <h4 class="card-title text-center  mb-4"><?= $pro; ?> <i class="fa fa-book fa-lg" style="color:#4581E0"></i></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card ">
                                <div class="card-header ">
                                    <h4 class="card-title">Email Statistics</h4>
                                    <p class="card-category">Last Campaign Performance</p>
                                </div>
                                <div class="card-body ">
                                    <div id="chartPreferences" class="ct-chart ct-perfect-fourth"></div>
                                    <div class="legend">
                                        <i class="fa fa-circle text-info"></i> Open
                                        <i class="fa fa-circle text-danger"></i> Bounce
                                        <i class="fa fa-circle text-warning"></i> Unsubscribe
                                    </div>
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-clock-o"></i> Campaign sent 2 days ago
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card ">
                                <div class="card-header ">
                                    <h4 class="card-title">Email Statistics</h4>
                                    <p class="card-category">Last Campaign Performance</p>
                                </div>
                                
                                <div class="card-body ">
                                    <div id="chartPreferences" class="ct-chart ct-perfect-fourth"></div>
                                    <div class="legend">
                                        <i class="fa fa-circle text-info"></i> Open
                                        <i class="fa fa-circle text-danger"></i> Bounce
                                        <i class="fa fa-circle text-warning"></i> Unsubscribe
                                    </div>
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-clock-o"></i> Campaign sent 2 days ago
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card ">
                                <div class="card-header ">
                                    <h4 class="card-title">Users Behavior</h4>
                                    <p class="card-category">24 Hours performance</p>
                                </div>
                                <div class="chart-container">
                                    <canvas id="graphCanvas"></canvas>
                                </div>
                                <div class="card-footer ">
                                    <div class="legend">
                                        <i class="fa fa-circle text-info"></i> Open
                                        <i class="fa fa-circle text-danger"></i> Click
                                        <i class="fa fa-circle text-warning"></i> Click Second Time
                                    </div>
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-history"></i> Updated 3 minutes ago
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card strpied-tabled-with-hover">
                                <div class="card-header ">
                                    <h4 class="card-title">จำนวนศิษย์เก่าแยกตามคณะวิชา</h4>
                                    <!-- <p class="card-category">อัพเดทข้อมูลล่าสุดเมื่อ</p> -->
                                </div>
                                <?php
                                $query = "SELECT * FROM faculty ORDER BY fac_id asc";
                                $result = mysqli_query($conn, $query);
                                $arr_fac = [];
                                $i = 1;
                                if ($result->num_rows > 0) {
                                    $arr_fac = $result->fetch_all(MYSQLI_ASSOC);
                                }
                                ?>
                                <div class="card-body table-full-width table-responsive">
                                    <table class="table table-hover table-striped">
                                        <thead>
                                            <th style="width:5%">#</th>
                                            <th style="width:80%">ชื่อคณะวิชา</th>
                                            <th style="width:15%">จำนวนศิษย์เก่า</th>
                                        </thead>
                                        <tbody>
                                        <?php if (!empty($arr_fac)) { ?>
                                                <?php foreach ($arr_fac as $fac) { ?>
                                                    <tr>
                                                        <td><?= $i ?></td>
                                                        <td><?= $fac["fac_name"] ?></td>
                                                        <td>
                                                           0
                                                        </td>
                                                        
                                                    </tr>
                                                <?php $i++;
                                                } ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <?php include_once("../../components/footer.inc.php") ?>
        </div>
    </div>
</body>

<script src="https://code.jquery.com/jquery-3.5.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="../../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../../assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="../../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/js/plugins/bootstrap-switch.js"></script>
<script src="../../assets/js/plugins/bootstrap-notify.js"></script>
<script src="../../assets/js/light-bootstrap-dashboard.js?v=2.0.0 " type="text/javascript"></script>
<script src="../../assets/js/demo.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.2/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.2/js/dataTables.bootstrap4.min.js"></script>
<script src="../../assets/script.js"></script>
<script>
    $(document).ready(function() {
        showGraph();
    });

    function showGraph() {
        {
            $.post("data.php", function(data) {
                console.log(data);
                let fac = [];
                let id = [];
                for (let i in data) {
                    fac.push(data[i].fac_name);
                    id.push(data[i].fac_id);
                }
                let chartdata = {
                    labels: fac,
                    datasets: [{
                        label: 'Faculty',
                        backgroundColor: '#87d4f1',
                        borderColor: '#46d5f1',
                        hoverBackgroundColor: '#da9744',
                        hoverBorderColor: '#666666',
                        data: id
                    }]
                };
                let graphTarget = $('#graphCanvas');
                let barGraph = new Chart(graphTarget, {
                    type: 'bar',
                    data: chartdata
                })
            })
        }
    }
</script>

</html>