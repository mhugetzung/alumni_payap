<?php
session_start();
include('../../../controller/config.php');

if(!isset($_SESSION['admin_id'])) {
    header("Location: ../../controller/auth/login.php");
}
