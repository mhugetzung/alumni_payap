<?php
require_once("../auth.inc.php");
?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/logo.png">
    <link rel="icon" type="image/png" href="../../assets/img/logo.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>จัดการข้อมูลคณะวิชา | มหาวิทยาลัยพายัพ</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <link href="../../assets/css/demo.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Mitr&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.11.2/css/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <?php include_once("../../components/sidebar.inc.php") ?>
        <div class="main-panel">
            <?php include_once("../../components/navbar.inc.php") ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card strpied-tabled-with-hover">
                                <div class="card-header d-flex justify-content-between">
                                    <h4 class="card-title">จัดการคณะวิชา</h4>
                                    <a class="btn btn-primary mb-3" href="create.php" role="button">เพิ่มข้อมูล</a>
                                </div>
                                <!-- <div class="card-body table-full-width table-responsive"> -->
                                <?php
                                $query = "SELECT * FROM faculty ORDER BY fac_id asc";
                                $result = mysqli_query($conn, $query);
                                $arr_fac = [];
                                $i = 1;
                                if ($result->num_rows > 0) {
                                    $arr_fac = $result->fetch_all(MYSQLI_ASSOC);
                                }
                                ?>
                                <div class="card-body table-full-width table-responsive">
                                    <table class="table table-hover table-striped" id="dataTable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>ชื่อคณะวิชา</th>
                                                <th>แก้ไข</th>
                                                <th>ลบ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (!empty($arr_fac)) { ?>
                                                <?php foreach ($arr_fac as $fac) { ?>
                                                    <tr>
                                                        <td><?= $i ?></td>
                                                        <td><?= $fac["fac_name"] ?></td>
                                                        <td>
                                                            <a href="edit.php?fac_id=<?= $fac['fac_id'] ?>" class="text-primary">
                                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="../../controller/faculty/faculty_delete.php?fac_id=<?= $fac['fac_id'] ?>" onclick="return confirm('โปรดยืนยันเพื่อลบข้อมูล')" class="text-primary">
                                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php $i++;
                                                } ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include_once("../../components/footer.inc.php") ?>
        </div>
    </div>

    <script src="../../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../assets/js/plugins/bootstrap-switch.js"></script>
    <script src="../../assets/js/plugins/bootstrap-notify.js"></script>
    <script src="../../assets/js/light-bootstrap-dashboard.js?v=2.0.0 " type="text/javascript"></script>
    <script src="../../assets/js/demo.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.2/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.2/js/dataTables.bootstrap4.min.js"></script>
    <script src="../../assets/script.js"></script>
</body>

</html>