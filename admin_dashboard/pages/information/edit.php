<?php
require_once("../auth.inc.php");
if (isset($_GET['inf_id'])) {
    $inf_id = $_GET['inf_id'];

    $query = "SELECT * FROM information WHERE inf_id = '$inf_id'";
    $result = mysqli_query($conn, $query) or die("Error in query: $sql " . mysqli_error($conn));
    $inf = mysqli_fetch_assoc($result);

    // print_r($fac);
    // return;
} else {
    header("Location: index.php");
}
?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/logo.png">
    <link rel="icon" type="image/png" href="../../assets/img/logo.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>แก้ไขข้อมูลหน้าเว็บฯ | มหาวิทยาลัยพายัพ</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <link href="../../assets/css/demo.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Mitr&display=swap" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <?php include_once("../../components/sidebar.inc.php") ?>
        <div class="main-panel">
            <?php include_once("../../components/navbar.inc.php") ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <h4 class="card-title">แก้ไขข้อมูลหน้าเว็บไซต์</h4>
                        </div>
                        <div class="card-body">
                            <form action="../../controller/information/information_update.php?inf_id=<?= $inf['inf_id'] ?>" method="post" enctype="multipart/form-data">
                                <div class="col">
                                    <div class="form-group">
                                        <label style=color:red;>* </label>&nbsp;<label>หัวข้อแสดงหน้าเว็บไซต์ภาษาไทย 1</label>
                                        <input type="text" class="form-control" name="inf_ti1" placeholder="กรอกหัวข้อแสดงหน้าเว็บไซต์ภาษาไทย" onkeypress="return " value="<?= $inf['inf_ti1'] ?>" required maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label style=color:red;>* </label>&nbsp;<label>หัวข้อแสดงหน้าเว็บไซต์ภาษาอังกฤษ 1</label>
                                        <input type="text" class="form-control" name="inf_ti1_en" placeholder="กรอกหัวข้อแสดงหน้าเว็บไซต์ภาษาอังกฤษ" onkeypress="return " value="<?= $inf['inf_ti1_en'] ?>" required maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label style=color:red;>* </label>&nbsp;<label>รายละเอียดย่อยภาษาไทย 1</label>
                                        <input type="text" class="form-control" name="inf_det1" placeholder="กรอกรายละเอียดภาษาไทย" onkeypress="return " value="<?= $inf['inf_det1'] ?>" required maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label style=color:red;>* </label>&nbsp;<label>รายละเอียดย่อยภาษาอังกฤษ 1</label>
                                        <input type="text" class="form-control" name="inf_det1_en" placeholder="กรอกรายละเอียดภาษาอังกฤษ" onkeypress="return " value="<?= $inf['inf_det1_en'] ?>" required maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>รายละเอียดย่อยภาษาไทย 2</label>
                                        <input type="text" class="form-control" name="inf_det2" placeholder="กรอกรายละเอียดภาษาไทย" onkeypress="return " value="<?= $inf['inf_det2'] ?>" maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>รายละเอียดย่อยภาษาอังกฤษ 2</label>
                                        <input type="text" class="form-control" name="inf_det2_en" placeholder="กรอกรายละเอียดภาษาอังกฤษ" onkeypress="return " value="<?= $inf['inf_det2_en'] ?>" maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>รายละเอียดย่อยภาษาไทย 3</label>
                                        <input type="text" class="form-control" name="inf_det3" placeholder="กรอกรายละเอียดภาษาไทย" onkeypress="return " value="<?= $inf['inf_det3'] ?>" maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>รายละเอียดย่อยภาษาอังกฤษ 3</label>
                                        <input type="text" class="form-control" name="inf_det3_en" placeholder="กรอกรายละเอียดภาษาอังกฤษ" onkeypress="return " value="<?= $inf['inf_det3_en'] ?>" maxlength="100">
                                    </div>
                                </div>
                                <hr width=100% color="white">
                                <div class="col">
                                    <div class="form-group">
                                        <label style=color:red;>* </label>&nbsp;<label>หัวข้อแสดงหน้าเว็บไซต์ภาษาไทย 2</label>
                                        <input type="text" class="form-control" name="inf_ti2" placeholder="กรอกหัวข้อแสดงหน้าเว็บไซต์ภาษาไทย" onkeypress="return " value="<?= $inf['inf_ti2'] ?>" required maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label style=color:red;>* </label>&nbsp;<label>หัวข้อแสดงหน้าเว็บไซต์ภาษาอังกฤษ 2</label>
                                        <input type="text" class="form-control" name="inf_ti2_en" placeholder="กรอกหัวข้อแสดงหน้าเว็บไซต์ภาษาอังกฤษ" onkeypress="return " value="<?= $inf['inf_ti2_en'] ?>" required maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label style=color:red;>* </label>&nbsp;<label>รายละเอียดย่อยภาษาไทย 1</label>
                                        <input type="text" class="form-control" name="inf_det4" placeholder="กรอกรายละเอียดภาษาไทย" onkeypress="return " value="<?= $inf['inf_det4'] ?>" required maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label style=color:red;>* </label>&nbsp;<label>รายละเอียดย่อยภาษาอังกฤษ 1</label>
                                        <input type="text" class="form-control" name="inf_det4_en" placeholder="กรอกรายละเอียดภาษาอังกฤษ" onkeypress="return " value="<?= $inf['inf_det4_en'] ?>" required maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>รายละเอียดย่อยภาษาไทย 2</label>
                                        <input type="text" class="form-control" name="inf_det5" placeholder="กรอกรายละเอียดภาษาไทย" onkeypress="return " value="<?= $inf['inf_det5'] ?>" maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>รายละเอียดย่อยภาษาอังกฤษ 2</label>
                                        <input type="text" class="form-control" name="inf_det5_en" placeholder="กรอกรายละเอียดภาษาอังกฤษ" onkeypress="return " value="<?= $inf['inf_det5_en'] ?>" maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>รายละเอียดย่อยภาษาไทย 3</label>
                                        <input type="text" class="form-control" name="inf_det6" placeholder="กรอกรายละเอียดภาษาไทย" onkeypress="return " value="<?= $inf['inf_det6'] ?>" maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>รายละเอียดย่อยภาษาอังกฤษ 3</label>
                                        <input type="text" class="form-control" name="inf_det6_en" placeholder="กรอกรายละเอียดภาษาอังกฤษ" onkeypress="return " value="<?= $inf['inf_det6_en'] ?>" maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                    <center><img width="500" height="300" src="../../../images/information/<?= $inf['inf_img'] ?>" alt=""></center><br>
                                        <label style=color:red;>* </label>&nbsp;<label>รูปภาพ</label>
                                        <input type="file" class="form-control" name="inf_img"   required >
                                    </div>
                                </div>
                                
                                <br>
                                <div class="col">
                                    <button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
                                    <a class="btn btn-primary" href="index.php" role="button">ยกเลิก</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php include_once("../../components/footer.inc.php") ?>
        </div>
    </div>

    <script src="../../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../assets/js/plugins/bootstrap-switch.js"></script>
    <script src="../../assets/js/plugins/bootstrap-notify.js"></script>
    <script src="../../assets/js/light-bootstrap-dashboard.js?v=2.0.0 " type="text/javascript"></script>
    <script src="../../assets/js/demo.js"></script>
</body>

</html>