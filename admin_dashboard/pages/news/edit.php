<?php
require_once("../auth.inc.php");
if (isset($_GET['news_id'])) {
    $news_id = $_GET['news_id'];

    $query = "SELECT * FROM news WHERE news_id = '$news_id'";
    $result = mysqli_query($conn, $query) or die("Error in query: $sql " . mysqli_error($conn));
    $news = mysqli_fetch_assoc($result);

    // print_r($fac);
    // return;
} else {
    header("Location: index.php");
}
?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/logo.png">
    <link rel="icon" type="image/png" href="../../assets/img/logo.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>แก้ไขข้อมูลข่าวสารฯ | มหาวิทยาลัยพายัพ</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <link href="../../assets/css/demo.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Mitr&display=swap" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <?php include_once("../../components/sidebar.inc.php") ?>
        <div class="main-panel">
            <?php include_once("../../components/navbar.inc.php") ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <h4 class="card-title">แก้ไขข่าวสารประชาสัมพันธ์</h4>
                        </div>
                        <div class="card-body">
                            <form action="../../controller/news/news_update.php?news_id=<?= $news['news_id'] ?>" method="post" enctype="multipart/form-data">
                                <div class="col">
                                    <div class="form-group">
                                        <label style=color:red;>* </label>&nbsp;<label>หัวข้อข่าวสารประชาสัมพันธ์ภาษาไทย</label>
                                        <input type="text" class="form-control" name="news_title" placeholder="หัวข้อข่าวสารประชาสัมพันธ์ภาษาไทย" onkeypress="return " value="<?= $news['news_title'] ?>" required maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label style=color:red;>* </label>&nbsp;<label>หัวข้อข่าวสารประชาสัมพันธ์ภาษาอังกฤษ</label>
                                        <input type="text" class="form-control" name="news_title_en" placeholder="หัวข้อข่าวสารประชาสัมพันธ์ภาษาอังกฤษ" onkeypress="return " value="<?= $news['news_title_en'] ?>" required maxlength="100">
                                    </div>
                                </div>
                                <div class="col">
                                    <label style=color:red;>*</label>&nbsp;<label>รายละเอียดข่าวสารประชาสัมพันธ์ภาษาไทย</label>
                                    <textarea class="form-control" name="news_detail" style="height: 150px"><?= $news['news_detail'] ?></textarea>
                                </div><br>
                                <div class="col">
                                    <label style=color:red;>*</label>&nbsp;<label>รายละเอียดข่าวสารประชาสัมพันธ์ภาษาอังกฤษ</label>
                                    <textarea class="form-control" name="news_detail_en" style="height: 150px"><?= $news['news_detail_en'] ?></textarea>
                                </div><br>
                                <div class="col">
                                    <div class="form-group">
                                        <center><img width="500" height="300" src="../../../images/news/<?= $news['news_img'] ?>" alt=""></center><br>
                                        <label style=color:red;>* </label>&nbsp;<label>รูปภาพ</label>
                                        <input type="file" class="form-control" name="news_img" required>
                                    </div>
                                </div>

                                <br>
                                <div class="col">
                                    <button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
                                    <a class="btn btn-primary" href="index.php" role="button">ยกเลิก</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php include_once("../../components/footer.inc.php") ?>
        </div>
    </div>

    <script src="../../assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../assets/js/plugins/bootstrap-switch.js"></script>
    <script src="../../assets/js/plugins/bootstrap-notify.js"></script>
    <script src="../../assets/js/light-bootstrap-dashboard.js?v=2.0.0 " type="text/javascript"></script>
    <script src="../../assets/js/demo.js"></script>
</body>

</html>