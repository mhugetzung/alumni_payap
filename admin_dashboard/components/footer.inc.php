<footer class="footer">
                <div class="container-fluid">
                    <nav>

                        <p class="copyright text-center">
                            ©
                            <script>
                                document.write(new Date().getFullYear())
                            </script>
                            <a  href="https://www.payap.ac.th/_public/">Payap University</a>
                        </p>
                    </nav>
                </div>
            </footer>