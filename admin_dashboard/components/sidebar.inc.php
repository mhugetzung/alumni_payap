<div class="sidebar" data-color="blue">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="#" class="simple-text">
            <img src="../../../images/identity_white.png" width="153" height="50" alt="logo"><br>
                ALUMNI PAYAP
            </a>
        </div>
        <ul class="nav">
            <li class="nav-item active">
                <a class="nav-link" href="../dashboard/index.php">
                    <i class="nc-icon nc-chart-pie-35"></i>
                    <p>หน้าหลัก</p>
                </a>
            </li>
            <?php if ($_SESSION["admin_role_id"] == 1) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="../admin/index.php">
                        <i class="nc-icon nc-single-02"></i>
                        <p>จัดการผู้ดูแล</p>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="../user/index.php">
                        <i class="nc-icon nc-single-02"></i>
                        <p>จัดการศิษย์เก่า</p>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="../department/index.php">
                        <i class="nc-icon nc-single-copy-04"></i>
                        <p>จัดการสาขาวิชา</p>
                    </a>
                </li> 
                <li>
                    <a class="nav-link" href="../faculty/index.php">
                        <i class="nc-icon nc-single-copy-04"></i>
                        <p>จัดการคณะวิชา</p>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="../program/index.php">
                        <i class="nc-icon nc-single-copy-04"></i>
                        <p>จัดการหลักสูตร</p>
                    </a>
                </li>
            <?php  } else if ($_SESSION["admin_role_id"] == 2) { ?>
                <li>
                    <a class="nav-link" href="../user/index.php">
                        <i class="nc-icon nc-single-02"></i>
                        <p>จัดการศิษย์เก่า</p>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="../news/index.php">
                        <i class="nc-icon nc-paper-2"></i>
                        <p>จัดการข่าวสารฯ</p>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="../information/edit.php?inf_id=1">
                        <i class="nc-icon nc-paper-2"></i>
                        <p>จัดการข้อความหน้าเว็บไซต์</p>
                    </a>
                </li>
            <?php  } else { ?>
            <?php } ?>
            <?php
                $sql_admin = "SELECT admin_id, admin_no, admin_username, admin_password,
                admin_firstname, admin_lastname, admin_position, admin_email, admin_phone 
                FROM admin WHERE admin_id = '".$_SESSION["admin_id"]."'";
                $result = mysqli_query($conn, $sql_admin);                    
                $admin_profile = mysqli_fetch_assoc($result);
            ?>
            <hr width="190" color="white">
            <li style="text-align: center;">
                <a class="nav-link" href="../admin/editpro.php?admin_id=<?= $admin_profile['admin_id'] ?>">
                    <p>แก้ไขข้อมูลส่วนตัว</p>
                </a>
            </li>
            <li style="text-align: center;">
                <a class="nav-link" href="../admin/editpw.php?admin_id=<?= $admin_profile['admin_id'] ?>">
                    <p>เปลี่ยนรหัสผ่าน</p>
                </a>
            </li>
            <li style="text-align: center;">
                <a class="nav-link" href="../../../index.php">
                    <p>กลับสู่หน้าแรก</p>
                </a>
            </li>
            <?php  ?>

        </ul>
    </div>
</div>