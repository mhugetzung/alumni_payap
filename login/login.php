<?php
session_start();

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>เข้าสู่ระบบสำหรับศิษย์เก่า | ศิษย์เก่ามหาวิทยาลัยพายัพ</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="../images/logo.png" />
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<!-- <link href="https://fonts.googleapis.com/css2?family=Mitr&display=swap" rel="stylesheet"> -->
	<link href="https://fonts.googleapis.com/css2?family=Prompt:wght@300;400;500;700;800;900&display=swap" rel="stylesheet">
</head>

<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form action="../controller/auth/checklogin.php" method="POST" class="login100-form validate-form p-l-55 p-r-55 p-t-178">
					<span class="login100-form-title">
						<img src="../images/logo-login.png" width="90" height="90" alt="logo">
					</span>
					<br>
					<span class="txt1">
						รหัสนักศึกษา
					</span><br>
					<span class="txt4">
						* หากจำรหัสนักศึกษาไม่ได้สามารถค้นหาในทะเบียนศิษย์เก่า
					</span>
					<div class="wrap-input100 validate-input m-b-16" data-validate="กรุณากรอกรหัสนักศึกษา">
						<input class="input100" type="text" name="alumni_stuid" placeholder="">
						<span class="focus-input100"></span>
					</div>
					<span class="txt1">
						รหัสผ่าน
					</span><br>
					<span class="txt4">
						* รหัสผ่านในการเข้าสู่ระบบครั้งแรกคือ payap
					</span>
					<div class="wrap-input100 validate-input" data-validate="กรุณากรอกรหัสผ่าน">
						<input class="input100" type="password" name="alumni_password" placeholder="">
						<span class="focus-input100"></span>
					</div>

					<div class="flex-col-c p-t-40 p-b-20">
						<div class="container-login100-form-btn p-b-10">
							<input type="submit" name="submitlogin" class="login100-form-btn" value="เข้าสู่ระบบ">
						</div>
						<div class="container-login200-form-btn">
							<a href="../index.php">
								ย้อนกลับ
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<script src="js/main.js"></script>
</body>

</html>