<?php
define("HOME", "หน้าแรก");
define("ABOUT", "บริจาคให้พายัพ");
define("DistinguishedAlumni", "ศิษย์เก่าดีเด่น");
define("ALUMNI", "ทะเบียนศิษย์เก่า");
define("LOGIN", "เข้าสู่ระบบ");
define("TITLEHOME", "ศิษย์เก่า | มหาวิทยาลัยพายัพ");
define("FACULTY", "คณะวิชา");
define("FACULTYOFPHARMACY", "คณะเภสัชศาสตร์");
define("FACULTYOFHUMANITIESANDCOMMUNICATIONARTS", "คณะมุนษย์ศาสตร์และนิเทษศาสตร์");
define("FACULTYOFLAW", "คณะนิติศาสตร์");
define("FACULTYOFBUSINESSADMINISTRATION", "คณะบริหารธุรกิจ");
define("FACULTYOFACCOUNTINGANDDIGITALBUSINESS", "คณะบัญชี และธุรกิจดิจิทัล");
define("MCCORMICKFACULTYOFNURSING", "คณะพยาบาลศาสตร์แมคคอร์มิค");
define("MCGILVARYOFDIVINITY", "พระคริสต์ธรรมแมคกิลวารี");
define("DURIYASILPCOLLEGEOFMUSIC", "วิทยาลัยดุริยศิลป์");
define("THEINTERNATIONALCOLLEGE", "วิทยาลัยนานาชาติ");
define("CONTACTUS", "ติดต่อเรา");
define("OFFICE", "สำนัก/หน่วยงาน");
define("PAYAPMAEKAO", "มหาวิทยาลัยพายัพ (เขตแม่คาว) ถนนซุปเปอร์ไฮเวย์เชียงใหม่-ลำปาง อำเภอเมือง จังหวัดเชียงใหม่ 50000");
define("PAYAPKAEWNAWARAT", "มหาวิทยาลัยพายัพ (เขตแก้วนวรัฐ) ถนนแก้วนวรัฐ อำเภอเมือง จังหวัดเชียงใหม่ 50000");
define("TEL", "โทรศัพท์ : 053 851478 ถึง 86, 053 241255");
define("EMAILPAYAP", "อีเมล : contact@payap.ac.th");
define("OFFICEOFACADEMICANDRESEARCHDEPARTMENT", "สำนักงานฝ่ายวิชาการและวิจัย");
define("OFFICEOFMARKETINGANDCORPORATECOMMUNICATION", "สำนักการตลาดและสื่อสารองค์กร");
define("PAYAPINTERNATIONALAFFAIRS", "สำนักวิเทศสัมพันธ์");
define("OFFICEOFREGISTRARANDEDUCATIONSERVICE", "สำนักทะเบียนและบริการนักศึกษา");
define("OFFICEOFFINANCIAL", "สำนักยบริิหารการเงิน");
define("STUDENTAFFAIRS", "สำนักพัฒนานักศึกษา");
define("ALLDEP", "หน่วยงานทั้งหมด");
define("RecognizedAlumni", "ศิษย์เก่าที่ประสบความสำเร็จและได้รับการยอมรับ");
define("PUBLICINFORMATIONANDEVENT", "ข่าวสารประชาสัมพันธ์และกิจกรรม");
define("READALL", "อ่านข่าวทั้งหมด");


