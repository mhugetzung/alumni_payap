<?php
require_once("../config.php");
if (isset($_GET['alumni_id']) && isset($_POST)) {
    $alumni_id = $_GET['alumni_id'];
    $alumni_phone = $_POST["alumni_phone"] ?? '';
    $alumni_email = $_POST["alumni_email"] ?? '';
    $current_provinces = $_POST["current_provinces"] ?? '';
    $current_amphure = $_POST["current_amphure"] ?? '';
    $current_district = $_POST["current_district"] ?? '';
    $current_number = $_POST["current_number"] ?? '';
    $current_zipcode = $_POST["current_zipcode"] ?? '';
    $facebooklink = $_POST["facebooklink"] ?? '';
    $lineid = $_POST["lineid"] ?? '';
    $link = $_POST["link"] ?? '';
    $modified = date('y-m-d H:i:s');
    $created = date('y-m-d H:i:s');
    $alumni_fname = $_POST["alumni_fname"];
    $alumni_firstname = $_POST["alumni_firstname"];
    $alumni_lastname = $_POST["alumni_lastname"];
    $alumni_nickname = $_POST["alumni_nickname"];
    $alumni_gender = $_POST["alumni_gender"] ?? '';
    $alumni_birthday = $_POST["alumni_birthday"] ?? '';
    $alumni_job = $_POST["alumni_job"] ?? '';
    $alumni_job_position = $_POST["alumni_job_position"] ?? '';
    $alumni_office = $_POST["alumni_office"] ?? '';
    $job_exp = $_POST["job_exp"] ?? '';
    $alumni_description = $_POST["alumni_description"] ?? '';
    $showdetail = isset($_POST["showdetail"]) ? 1 : 0;
    $showaddress = isset($_POST["showaddress"]) ? 1 : 0;
    $showtel = isset($_POST["showtel"]) ? 1 : 0;
    $showemail = isset($_POST["showemail"]) ? 1 : 0;
    $showfacebook = isset($_POST["showfacebook"]) ? 1 : 0;
    $showidline = isset($_POST["showidline"]) ? 1 : 0;
    $showlink = isset($_POST["showlink"]) ? 1 : 0;

    date_default_timezone_set('Asia/Bangkok');
    $date = date("Ymd");

    if($_FILES['fileupload']['name'] != '') {
        $numrand = (mt_rand());
        $path = "../../images/alumni/";
        $alumni_img = strrchr($_FILES['fileupload']['name'], ".");
        
        $newname = $date . $numrand . $alumni_img;
        $path_copy = $path . $newname;
        $path_link = "../../images/alumni/" . $newname;
        move_uploaded_file($_FILES['fileupload']['tmp_name'],$path_copy);  	


        $sql = "UPDATE alumni
                SET alumni_phone = '$alumni_phone',
                    alumni_email = '$alumni_email',
                    current_provinces = '$current_provinces',
                    current_amphure = '$current_amphure',
                    current_district = '$current_district',
                    current_zipcode = '$current_zipcode',
                    current_number = '$current_number',
                    facebooklink = '$facebooklink',
                    lineid = '$lineid',
                    link = '$link',
                    modified = '$modified',
                    created = '$created',
                    alumni_fname = '$alumni_fname',
                    alumni_firstname = '$alumni_firstname',
                    alumni_lastname = '$alumni_lastname',
                    alumni_nickname = '$alumni_nickname',
                    alumni_gender = '$alumni_gender',
                    alumni_birthday = '$alumni_birthday',
                    alumni_img = '$newname',
                    showaddress = $showaddress,
                    showemail = $showemail,
                    showtel = $showtel,
                    showfacebook = $showfacebook,
                    showidline = $showidline,
                    showlink = $showlink
                WHERE alumni_id = '$alumni_id'";
    } else {
        

        $sql = "UPDATE alumni
                SET alumni_phone = '$alumni_phone',
                    alumni_email = '$alumni_email',
                    current_provinces = '$current_provinces',
                    current_amphure = '$current_amphure',
                    current_district = '$current_district',
                    current_zipcode = '$current_zipcode',
                    current_number = '$current_number',
                    facebooklink = '$facebooklink',
                    lineid = '$lineid',
                    link = '$link',
                    modified = '$modified',
                    created = '$created',
                    alumni_fname = '$alumni_fname',
                    alumni_firstname = '$alumni_firstname',
                    alumni_lastname = '$alumni_lastname',
                    alumni_nickname = '$alumni_nickname',
                    alumni_gender = '$alumni_gender',
                    alumni_birthday = '$alumni_birthday',
                    showaddress = $showaddress,
                    showemail = $showemail,
                    showtel = $showtel,
                    showfacebook = $showfacebook,
                    showidline = $showidline,
                    showlink = $showlink
                WHERE alumni_id = '$alumni_id'";
    }
    
    $result = mysqli_query($conn, $sql) or die("Error in query: $sql " . mysqli_error($conn));

    $prevQuery = "SELECT alumni_id FROM alumni_job WHERE alumni_id = '$alumni_id'";
    $prevResult = $conn->query($prevQuery);

    if ($prevResult->num_rows > 0) {
        // Update member data in the database
        $sqlup = "UPDATE alumni_job
        SET alumni_job = '$alumni_job',
        alumni_job_position = '$alumni_job_position',
        alumni_office = '$alumni_office',
        job_exp = '$job_exp',
        alumni_description = '$alumni_description',
        modified = '$modified',
        showdetail = $showdetail
        WHERE alumni_id = '$alumni_id'";

        $result1 = mysqli_query($conn, $sqlup) or die("Error in query: $sqlup " . mysqli_error($conn));
    } else {
        // Insert member data in the database

        $sqlin = "INSERT INTO alumni_job
        (alumni_id, alumni_job, alumni_job_position, alumni_office, job_exp, alumni_description, created,modified,showdetail) 
            VALUES ('$alumni_id', '$alumni_job', '$alumni_job_position', '$alumni_office', '$job_exp', '$alumni_description', '$created', '$modified', '$showdetail')";
        $result2 = mysqli_query($conn, $sqlin) or die("Error in query: $sqlin " . mysqli_error($conn));
    }

    echo "<script>";
    echo "alert('แก้ไขข้อมูลเรียบร้อย')";
    echo "</script>";

    header("Refresh:0; url=../../pages/alumni/detail.php?alumni_id=$alumni_id");
} else {
    header("Location: ../../pages/home/index.php");
}
mysqli_close($conn);
