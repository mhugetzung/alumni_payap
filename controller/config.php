<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "alumni_db";

//connection
date_default_timezone_set('Asia/Bangkok');
$thaimonth=array("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
$thaimonth_full=array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");

$date = date('Y-m');
$myDateTime = DateTime::createFromFormat('Y-m',$date); 
$m = $myDateTime->format('m');
$y = $myDateTime->format('Y');

$newdate = $myDateTime->format('Y-m');

//create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
mysqli_set_charset($conn,"utf8");

//check conection
if (!$conn){
  die("Connection failed: " . mysqli_connect_error());
}

function debug($array) {
  echo "<pre>";
  print_r($array);
  echo "</pre>";
}