<?php
session_start();
require_once("../../controller/config.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="../../images/logo.png">
	<title>ศิษย์เก่า | มหาวิทยาลัยพายัพ</title>
	<link href="../../plugins/bootstrap/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="../../plugins/themify-icons/themify-icons.css">
	<link href="../../plugins/slick/slick.css" rel="stylesheet">
	<link href="../../plugins/slick/slick-theme.css" rel="stylesheet">
	<link href="../../css/style.css" rel="stylesheet">
	<link href="../../images/favicon.png" rel="shortcut icon">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
	<style>
		.li-text {
			color: white;
		}
		.p-bold {
			color: black;

		}
		.p-text {
			color: black;
			font-size: 14px;
			font-weight: 300;
		}

		.p-textalumni {
			color: white;
		}

		.p-textalumnilo {
			color: white;
		}


		#img {
			width: 60%;
		}

		.allimg {
			margin: 7px;
			padding: 7px;
			width: 300px;
			height: 300px;
			float: left;
		}

		.img-show {
			width: 100%;
			height: 300px;
			object-fit: cover;

		}
	</style>
</head>

<body class="body-wrapper">
	<?php include_once("../../components/navbar.inc.php") ?>
	<!--Homepage Banner-->
	<section class="banner bg-1" id="home">
	</section>
	<section id="news"><br>
		<div class="container">
			<div class="row">
				<div class="mb-3 col-6">
					<h5 style="color:black;"><?=PUBLICINFORMATIONANDEVENT;?></h5>
				</div>
				<div class="mb-3 col-6" style="text-align: right;"><a style="color:#3472F7; " href="../news/index.php" target="_blank" aria-pressed="true"><?=READALL;?></a></div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<?php
				$query = "SELECT * FROM news order by news_id desc limit 0,1";
				$result = mysqli_query($conn, $query);
				while ($row = mysqli_fetch_array($result)) {
					$news_id = $row['news_id'];
					$news_title = $row['news_title'];
					$news_created_at = $row['news_created_at'];
					$news_img = $row['news_img'];
					$news_detail = mb_substr($row['news_detail'], 0, 700, 'UTF-8');
				?>
					<div class="card mb-4">
						<section class="blog_are section_padding_0_80">
							<div class="container">
								<div class="row  justify-content-center">
									<div class="col-12 col-lg-8">
										<div class="row">
											<div class="col-12">
												<div class="single-post wow fadeInUp" data-wow-delay=".2s">
													<div class="post-content mt-4">
														<h6 class="post-headline"><a style="color:black;" href="../news/page.php?news_id=<?= $news_id; ?>"><?= $news_title; ?></a> <span class="badge badge-danger">ข่าวใหม่</span></h6>
													</div>
													<div class="post-content">
														<p><small class="text-muted">เผยแพร่เมื่อ <?= substr($news_created_at, 8, 2) . " " . $thaimonth_full[substr($news_created_at, 5, 2) - 1] . " " . substr(substr($news_created_at, 0, 4) + 543, 2, 2) ?></small></p>
													</div>
													<div class="post-tumb">
														<img class="img-fluid my-element" src="../../images/news/<?= $news_img; ?>" alt="Card image cap">
													</div>
													<div class="post-content mt-4">
														<p style="color:black; text-align:justify"><?= $news_detail; ?></p>
													</div>
													<div class="post-content mb-2 mt-3">
														<center><a href="../news/page.php?news_id=<?= $news_id; ?>" target="_blank" class="btn btn-primary" role="button" aria-pressed="true">ดูรายละเอียด</a></center>
													</div>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
								</div>
							</div>
							<?php
							$query2 = "SELECT * FROM news order by news_id desc limit 1,3";
							$result2 = mysqli_query($conn, $query2);
							while ($row = mysqli_fetch_array($result2)) {
								$news_id = $row['news_id'];
								$news_title = $row['news_title'];
								$news_created_at = $row['news_created_at'];
								$news_img = $row['news_img'];
								$news_detail = mb_substr($row['news_detail'], 0, 700, 'UTF-8');
							?>
								<div class="col-md-4 float-left">
									<div class="single-post wow fadeInUp" data-wow-delay=".2s">
										<div class="post-content mt-4">
											<h6 class="post-headline"><a style="color:black;" href="../news/page.php?news_id=<?= $news_id; ?>"><?= $news_title; ?></a></h6>
										</div>
										<div class="post-content">
											<p><small class="text-muted">เผยแพร่เมื่อ <?= substr($news_created_at, 8, 2) . " " . $thaimonth_full[substr($news_created_at, 5, 2) - 1] . " " . substr(substr($news_created_at, 0, 4) + 543, 2, 2) ?></small></p>
										</div>
										<div class="post-tumb mb-3">
											<img class="img-fluid img-show " src="../../images/news/<?= $news_img; ?>" alt="Card image cap">
										</div>
									</div>
								</div>
							<?php } ?>
						</section>
					</div>
			</div>
		</div>
		<br><br><br><br><br><br>

	</section>

	<!--====  End of About  ====-->
	<section class="cta-download bg-3 overlay" id="about">
		<?php
		$query = "SELECT * FROM information order by inf_id desc limit 0,1";
		$result2 = mysqli_query($conn, $query);
		while ($row = mysqli_fetch_array($result2)) {
		?>
			<div class="container">
				<div class="row">
					<div class="col-lg-4 text-center mb-4">
						<div class="image"><img class="phone-thumb img-fluid" src="../../images/information/<?= $row['inf_img'] ?>" width="350" height="350" alt=""></div>
					</div>

					<div class="col-lg-8">
						<div class="content-block">
							<!-- Title -->
							<h4 style="text-align: center;"><?= $row['inf_ti1'] ?></h4>
							<!-- <h4 style="text-align: center;">ร่วมเป็นส่วนหนึ่งในการพัฒนามหาวิทยาลัยพายัพไปด้วยกัน</h4> -->
							<br>
							<!-- Desctcription -->
							<ul>
								<li class="li-text"><?= $row['inf_det1'] ?></li>
								<li class="li-text"><?= $row['inf_det2'] ?></li>
								<li class="li-text"><?= $row['inf_det3'] ?></li>
							</ul>
							<hr align="center" noshade color="white">
							<h4 style="text-align: center;"><?= $row['inf_ti2'] ?></h4>
							<br>
							<ul>
								<li class="li-text"><?= $row['inf_det4'] ?></li>
								<li class="li-text"><?= $row['inf_det5'] ?></li>
								<li class="li-text"><?= $row['inf_det6'] ?></li>
							</ul>
						</div>
					</div>
				<?php } ?>
				</div>
			</div>
	</section>
	<!--==============================
=            Features            =
===============================-->
	<section class="section feature" id="alumniaward">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section-title">
						<h4 style="color:black"><?=DistinguishedAlumni;?> <i class="fa fa-graduation-cap fa-lg"></i></h4>
						<p style="color:#4581E0"><?=RecognizedAlumni;?></p>
					</div>
				</div>
				<?php
				$query = "SELECT * FROM alumni
				LEFT JOIN faculty
           		ON alumni.fac_id = faculty.fac_id
				LEFT JOIN alumni_job
				ON alumni.alumni_id = alumni_job.alumni_id
				WHERE alumni.status = 1";
				$result2 = mysqli_query($conn, $query);
				while ($row = mysqli_fetch_array($result2)) {
					$alumni_id = $row['alumni_id'];
				?>
					<div class="col-md-4">
						<div class="card-deck">
							<div class="card">
								<img class="card-img-top img-show" src="../../images/alumni/<?= $row['alumni_img'] ?>" width="350" height="350" alt="Card image cap">
								<div class="card-body">
									<a style="color:black;" href="../alumni/page.php?alumni_id=<?= $alumni_id; ?>">
										<h5 class="card-title p-tex">คุณ<?= $row['alumni_firstname'] ?> <?= $row['alumni_lastname'] ?></h5>
										<p class="card-text p-bold"><?= $row['alumni_job'] ?></p>
										<p class="card-text p-text"><?= $row['alumni_office'] ?></p>
										<p class="card-text"><small class="text-muted"><?= $row['fac_name'] ?></small></p>

								</div>
							</div>
						</div><br>
					</div>

				<?php } ?>
			</div>
		</div>
		</div>
	</section>
	<?php include_once("../../components/footer.inc.php") ?>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI14J_PNWVd-m0gnUBkjmhoQyNyd7nllA" async defer></script>
	<script src="../../plugins/jquery/jquery.js"></script>
	<script src="../../plugins/bootstrap/bootstrap.min.js"></script>
	<script src="../../plugins/slick/slick.min.js"></script>
	<script src="../../js/custom.js"></script>
	<script src="../../assets/scriptimg.js"></script>
</body>

</html>