<?php
require_once("../auth.inc.php");
$alumni_id = $_GET['alumni_id'];

if(!isset($_SESSION['admin_id']) && $alumni_id != $_SESSION['alumni_id']) {
    header("Location: detail.php?alumni_id=$alumni_id");
}

$query = "SELECT * FROM alumni WHERE alumni_id = '$alumni_id'";
$result = mysqli_query($conn, $query) or die("Error in query: $sql " . mysqli_error($conn));
$alumni = mysqli_fetch_assoc($result);

$query2 = "SELECT * FROM alumni_job WHERE alumni_id = '$alumni_id'";
$result2 = mysqli_query($conn, $query2) or die("Error in query: $sql " . mysqli_error($conn));
$job = mysqli_fetch_assoc($result2);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../../images/logo.png">
    <title>ข้อมูลศิษย์เก่า | มหาวิทยาลัยพายัพ</title>
    <link href="../../plugins/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../plugins/themify-icons/themify-icons.css">
    <link href="../../plugins/slick/slick.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="../../plugins/slick/slick-theme.css" rel="stylesheet">
    <link href="../../css/style.css" rel="stylesheet">
    <link href="../../images/favicon.png" rel="shortcut icon">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="../../assets/datedropper-javascript.js"></script>
    <style>
        .a-text {
            color: #3472F7;
        }

        .p-text {
            color: black;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
        }

        .p-titletext {
            color: white;
        }

        .text {
            color: black;
        }

        :root {
            --dd-overlay: rgba(0, 0, 0, .75);
            --dd-background: #FFFFFF;
            --dd-text1: #333333;
            --dd-text2: #FFFFFF;
            --dd-primary: #FD4741;
            --dd-gradient: linear-gradient(45deg, #1D62F0 0%, #4581E0 100%);
            --dd-radius: .35em;
            --dd-shadow: 0 0 2.5em rgba(0, 0, 0, 0.1);
            --dd-range: rgba(0, 0, 0, 0.05);
        }
    </style>
</head>

<body class="body-wrapper">
    <?php include_once("../../components/navbar.inc.php") ?>
    <!--Homepage Banner-->
    <section class="banner bg-regalumni" id="home">
    </section>
    <section>
        <div class="breadcumb-nav">
            <div class="container">
                <div class="row">
                    <div class="mt-3">
                        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก</a></li>
                                <li class="breadcrumb-item"><a href="index.php">ทะเบียนศิษย์เก่า</a></li>
                                <li class="breadcrumb-item active" aria-current="page">ข้อมูลศิษย์เก่า</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="mb-3 col-6">
                <h5>ข้อมูลศิษย์เก่า</h5>
            </div>
        </div>
    </div>
    <?php
    if (isset($_GET['alumni_id'])) {
        $alumni_id = $_GET['alumni_id'];
        $query = "SELECT * FROM alumni
        LEFT JOIN faculty
        ON alumni.fac_id = faculty.fac_id
        LEFT JOIN program
        ON alumni.pro_id = program.pro_id
        LEFT JOIN department
        ON alumni.dep_id = department.dep_id
        WHERE alumni.alumni_id = '$alumni_id'";
        $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_array($result)) {
            $alumni_id = $row['alumni_id'];
            $alumni_stuid = $row['alumni_stuid'];
            $alumni_fname = $row['alumni_fname'];
            $alumni_firstname = $row['alumni_firstname'];
            $alumni_lastname = $row['alumni_lastname'];
            $alumni_nickname = $row['alumni_nickname'];
            $alumni_gender = $row['alumni_gender'];
            $alumni_phone = $row['alumni_phone'];
            $alumni_email = $row['alumni_email'];
            $alumni_img = $row['alumni_img'];
            $dep_id = $row['dep_id'];
            $fac_id = $row['fac_id'];
            $pro_id = $row['pro_id'];


    ?>
            <section class="blog_are section_padding_0_80">
                <div class="col-12">
                    <div class="card mb-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-4" style="text-align:center">
                                    <div class="post-content mt-4 mb-4">
                                        <img class="img-fluid" src="../../images/alumni/<?= $alumni_img; ?>" alt="Card image cap">
                                    </div>
                                    <div>
                                        <h6 class="text-dark"><?= $alumni_fname . ' ' . $alumni_firstname . ' ' . $alumni_lastname; ?></h6>
                                    </div>
                                    <form action="../../controller/alumni/alumni_update.php?alumni_id=<?= $alumni['alumni_id'] ?>" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label style=color:red;>* </label>&nbsp;<label class="text mb-2">เลือกรูปภาพ</label>
                                            <input type="file" class="form-control" name="fileupload" id="fileupload" accept="image/*">
                                        </div>
                                        <div>
                                            <a class="a-text" href="detail.php?alumni_id=<?= $alumni_id; ?>">ย้อนกลับ</a>
                                        </div>
                                        <br>
                                </div>
                                <div class="col-md-8">
                                    <div class="post-content">
                                        <div class="card mt-4" style="background:#4581E0">
                                            <p class="p-titletext">ข้อมูลส่วนบุคคล</p>
                                        </div>
                                        <?php
                                        $sql_provinces = "SELECT * FROM provinces ORDER BY CONVERT (name_th USING tis620) ASC";
                                        $query = mysqli_query($conn, $sql_provinces);
                                        ?>

                                        <div class="form-row mt-4">
                                            <div class="form-group col-md-4">
                                                <label style=color:red;>* </label>&nbsp;<label class="text" for="alumni_fname">คำนำหน้าชื่อ</label>
                                                <select class="form-control" name="alumni_fname" aria-label="Default select example">
                                                    <option>เลือกคำนำหน้าชื่อ</option>
                                                    <option value="นาย" <?= $row['alumni_fname'] == 'นาย' ? 'selected' : ''?>>นาย</option>
                                                    <option value="นาง" <?= $row['alumni_fname'] == 'นาง' ? 'selected' : ''?>>นาง</option>
                                                    <option value="นางสาว" <?= $row['alumni_fname'] == 'นางสาว' ? 'selected' : ''?>>นางสาว</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label style=color:red;>* </label>&nbsp;<label class="text" for="alumni_firstname">ชื่อ</label>
                                                <input type="text" name="alumni_firstname" id="alumni_firstname" class="form-control" required placeholder="กรอกชื่อ" value="<?= $alumni['alumni_firstname'] ?>">
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label style=color:red;>* </label>&nbsp;<label class="text" for="alumni_lastname">นามสกุล</label>
                                                <input type="text" name="alumni_lastname" id="alumni_lastname" class="form-control" required placeholder="กรอกนามสกุล" value="<?= $alumni['alumni_lastname'] ?>">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="text" for="alumni_stuid">รหัสนักศึกษา</label>
                                                <input type="text" name="alumni_stuid" id="alumni_stuid" class="form-control" placeholder="กรอกรหัสนักศึกษา" value="<?= $alumni['alumni_stuid'] ?>" disabled>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="text" for="alumni_nickname">ชื่อเล่น</label>
                                                <input type="text" name="alumni_nickname" id="alumni_nickname" class="form-control" placeholder="กรอกชื่อเล่น" value="<?= $alumni['alumni_nickname'] ?>">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="text" for="alumni_gender">เพศ</label>
                                                <select class="form-control" name="alumni_gender" aria-label="Default select example">
                                                    <option>เลือกเพศ</option>
                                                    <option value="ชาย" <?= $row['alumni_gender'] == 'ชาย' ? 'selected' : '' ?>>ชาย</option>
                                                    <option value="หญิง" <?= $row['alumni_gender'] == 'หญิง' ? 'selected' : '' ?>>หญิง</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="text" for="alumni_birthday">วันเกิด</label> <i class="fa fa-calendar" aria-hidden="true"></i>
                                                <input type="date" name="alumni_birthday" id="alumni_birthday" class="form-control" placeholder="เลือกวันเดือนปีเกิด" value="<?= $alumni['alumni_birthday'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <hr width="100%" color="white">
                                <div class="post-content">
                                    <div class="card" style="background:#4581E0">
                                        <p class="p-titletext">ข้อมูลสำหรับติดต่อศิษย์เก่า</p>
                                    </div>

                                    <div class="form-row mt-4">
                                        <div class="form-group col-md-4">
                                            <label class="text" for="province">จังหวัด</label>
                                            <select name="current_provinces" id="province" class="form-control">
                                                <option value="">เลือกจังหวัด</option>
                                                <?php while ($result = mysqli_fetch_assoc($query)) : ?>
                                                    <option value="<?= $result['id'] ?>"><?= $result['name_th'] ?></option>
                                                <?php endwhile; ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="text" for="amphure">อำเภอ/เขต</label>
                                            <select name="current_amphure" id="amphure" class="form-control">
                                                <option value="">เลือกอำเภอ/เขต</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="text" for="district">ตำบล/แขวง</label>
                                            <select name="current_district" id="district" class="form-control">
                                                <option value="">เลือกตำบล/แขวง</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="text" for="current_zipcode">รหัสไปรษณีย์</label>
                                            <input type="text" name="current_zipcode" id="current_zipcode" class="form-control" placeholder="กรอกรหัสไปรษณีย์" value="<?= $alumni['current_zipcode'] ?>">
                                            <div class="form-check mt-2">
                                                <input class="form-check-input" type="checkbox" name="showaddress" <?= $alumni['showaddress'] ? 'checked' : '' ?> id="flexCheckDefault">
                                                <label class="form-check-label text" for="flexCheckDefault">
                                                    ยินยอมให้เปิดเผยที่อยู่
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="text" for="current_number">บ้านเลขที่ / หมู่ที่</label>
                                            <input type="text" name="current_number" id="current_number" class="form-control" placeholder="กรอกบ้านเลขที่ / หมู่บ้าน" value="<?= $alumni['current_number'] ?>">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label class="text" for="phone">เบอร์โทรศัพท์</label>
                                            <input type="text" name="alumni_phone" id="phone" class="form-control" placeholder="กรอกเบอร์โทรศัพท์" maxlength="10" value="<?= $alumni['alumni_phone'] ?>">
                                            <div class="form-check mt-2">
                                                <input class="form-check-input" type="checkbox" name="showtel" <?= $alumni['showtel'] ? 'checked' : '' ?> id="flexCheckDefault">
                                                <label class="form-check-label text" for="flexCheckDefault">
                                                    ยินยอมให้เปิดเผยเบอร์โทรศัพท์
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label class="text" for="email">อีเมล</label>
                                            <input type="text" name="alumni_email" id="email" class="form-control" placeholder="กรอกอีเมล" value="<?= $alumni['alumni_email'] ?>">
                                            <div class="form-check mt-2">
                                                <input class="form-check-input" type="checkbox" name="showemail" <?= $alumni['showemail'] ? 'checked' : '' ?> id="flexCheckDefault">
                                                <label class="form-check-label text" for="flexCheckDefault">
                                                    ยินยอมให้เปิดเผยอีเมล
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label class="text" for="facebooklink">Facebook</label>
                                            <input type="text" name="facebooklink" id="facebooklink" class="form-control" placeholder="กรอกเฟสบุค" value="<?= $alumni['facebooklink'] ?>">
                                            <div class="form-check mt-2">
                                                <input class="form-check-input" type="checkbox" name="showfacebook"  <?= $alumni['showfacebook'] ? 'checked' : '' ?> id="flexCheckDefault">
                                                <label class="form-check-label text" for="flexCheckDefault">
                                                    ยินยอมให้เปิดเผยเฟสบุค
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label class="text" for="lineid">Line</label>
                                            <input type="text" name="lineid" id="lineid" class="form-control" placeholder="กรอกไอดีไลน์" value="<?= $alumni['lineid'] ?>">
                                            <div class="form-check mt-2">
                                                <input class="form-check-input" type="checkbox" name="showidline" <?= $alumni['showidline'] ? 'checked' : '' ?> id="flexCheckDefault">
                                                <label class="form-check-label text" for="flexCheckDefault">
                                                    ยินยอมให้เปิดเผยไอดีไลน์
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label class="text" for="link">ลิงค์ส่วนตัว</label>
                                            <input type="text" name="link" id="link" class="form-control" placeholder="กรอกลิงค์ส่วนตัว" value="<?= $alumni['link'] ?>">
                                            <div class="form-check mt-2">
                                                <input class="form-check-input" type="checkbox" name="showlink" <?= $alumni['showlink'] ? 'checked' : '' ?> id="flexCheckDefault">
                                                <label class="form-check-label text" for="flexCheckDefault">
                                                    ยินยอมให้เปิดเผยลิงค์ส่วนตัว
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <hr width="100%" color="white">

                                    <div class="post-content">
                                        <div class="card" style="background:#4581E0">
                                            <p class="p-titletext">ข้อมูลการทำงาน</p>
                                        </div>
                                        <p class="p-text ">อาชีพ</p>
                                        <input type="text" class="form-control" name="alumni_job" placeholder="กรอกอาชีพ" onkeypress="return " maxlength="100" value="<?= $job['alumni_job'] ?? ''  ?>">
                                        <p class="p-text ">ตำแหน่ง</p>
                                        <input type="text" class="form-control" name="alumni_job_position" placeholder="กรอกตำแหน่ง" onkeypress="return " maxlength="100" value="<?= $job['alumni_job_position'] ?? ''  ?>">
                                        <p class="p-text ">สถานที่ทำงาน</p>
                                        <textarea class="form-control" name="alumni_office" style="height: 100px"> <?= $job['alumni_office'] ?? '' ?></textarea>
                                        <p class="p-text ">ประสบการณ์ทำงาน / รางวัลความสำเร็จ</p>
                                        <textarea class="form-control" name="job_exp" style="height: 100px"> <?= $job['job_exp'] ?? '' ?></textarea>
                                        <p class="p-text ">คำแนะนำจากศิษย์เก่า</p>
                                        <textarea class="form-control" name="alumni_description" style="height: 100px"> <?= $job['alumni_description'] ?? '' ?></textarea>

                                        <div class="form-check mt-3 mb-4">
                                            <input class="form-check-input" type="checkbox" name="showdetail" <?= isset($job['showdetail']) && $job['showdetail'] ? 'checked' : '' ?> id="flexCheckDefault">
                                            <label class="form-check-label text" for="flexCheckDefault">
                                                ยินยอมให้เปิดเผยข้อมูลการทำงาน
                                            </label>
                                        </div>
                                        <center>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-success">บันทึก</button>
                                                <a class="btn btn-primary" href="detail.php?alumni_id=<?= $alumni_id; ?>" role="button">ยกเลิก</a>
                                            </div>
                                        </center>
                                        </form>
                                    </div>
                                </div>
                                </div>
                            </div>
                        <?php  }
                        ?>
            </section>
            </div>
            <?php include_once("../../components/footer.inc.php") ?>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI14J_PNWVd-m0gnUBkjmhoQyNyd7nllA" async defer></script>
            <script src="../../plugins/jquery/jquery.js"></script>
            <script src="../../plugins/bootstrap/bootstrap.min.js"></script>
            <script src="../../plugins/slick/slick.min.js"></script>
            <script src="../../js/custom.js"></script>
            <script src="../../assets/jquery.min.js"></script>
            <script src="../../assets/script.js"></script>
            <script>
                new dateDropper({
                    selector: 'input[type="date"]',
                    expandable: true,
                    expandedDefault: true,
                    format: 'dd-M-y'
                });
            </script>
</body>

</html>
<?php
mysqli_close($conn);
?>