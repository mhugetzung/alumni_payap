<?php
require_once("../auth.inc.php");
$alumni_id = $_GET['alumni_id'];

if(!isset($_SESSION['admin_id']) && $alumni_id != $_SESSION['alumni_id']) {
    header("Location: detail.php?alumni_id=$alumni_id");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../../images/logo.png">
    <title>ข้อมูลศิษย์เก่า | มหาวิทยาลัยพายัพ</title>
    <link href="../../plugins/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../plugins/themify-icons/themify-icons.css">
    <link href="../../plugins/slick/slick.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="../../plugins/slick/slick-theme.css" rel="stylesheet">
    <link href="../../css/style.css" rel="stylesheet">
    <link href="../../images/favicon.png" rel="shortcut icon">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <style>
        .a-text {
            color: #3472F7;
        }

        .text {
            color: black;
        }

        .btn-primary {
            border-color: #3472F7;
            color: #3472F7;
        }

        .btn {
            border-width: 2px;
            background-color: transparent;
            font-weight: 400;
            opacity: 0.8;
            filter: alpha(opacity=80);
            padding: 8px 16px;
            border-color: #3472F7;
            color: #3472F7;
        }

        .p-text {
            color: white;
        }
    </style>
</head>

<body class="body-wrapper">
    <?php include_once("../../components/navbar.inc.php") ?>
    <!--Homepage Banner-->
    <section class="banner bg-regalumni" id="home">
    </section>
    <section>
        <div class="breadcumb-nav">
            <div class="container">
                <div class="row">
                    <div class="mt-3">
                        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก</a></li>
                                <li class="breadcrumb-item"><a href="index.php">ทะเบียนศิษย์เก่า</a></li>
                                <li class="breadcrumb-item"><a href="faculty.php?fac_id=1">คณะวิชา</a></li>
                                <li class="breadcrumb-item active" aria-current="page">ข้อมูลศิษย์เก่า</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="mb-3 col-6">
                <h5>ข้อมูลศิษย์เก่า</h5>
            </div>
        </div>
    </div>
    <?php
    if (isset($_GET['alumni_id'])) {
        $alumni_id = $_GET['alumni_id'];
        $query = "SELECT *
    FROM alumni WHERE alumni_id = '$alumni_id'";
        $result = mysqli_query($conn, $query);
        while ($row = mysqli_fetch_array($result)) {
            $alumni_id = $row['alumni_id'];
            $alumni_stuid = $row['alumni_stuid'];
            $alumni_fname = $row['alumni_fname'];
            $alumni_firstname = $row['alumni_firstname'];
            $alumni_lastname = $row['alumni_lastname'];
            $alumni_img = $row['alumni_img'];
    ?>
            <section class="blog_are section_padding_0_80">
                <div class="col-12">
                    <div class="card mb-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-4" style="text-align:center">
                                    <div class="post-content mt-4 mb-4">
                                        <img class="img-fluid" src="../../images/alumni/<?= $alumni_img; ?>" width:380px; height:250px; alt="Card image cap">
                                    </div>
                                    <div>
                                        <h6 class="text-dark"><?= $alumni_fname . ' ' . $alumni_firstname . ' ' . $alumni_lastname; ?></h6>
                                    </div>
                                <?php } ?>
                                <div>
                                    <a class="a-text" href="detail.php?alumni_id=<?= $alumni_id; ?>">ย้อนกลับ</a>
                                </div>
                                <br>
                                </div>
                                <div class="col-sm-8">
                                    <div class="post-content">
                                        <div class="card mt-4" style="background:#4581E0">
                                            <p class="p-text">เปลี่ยนรหัสผ่าน</p>
                                        </div>
                                        <form action="../../controller/alumni/changepassword.php" method="post">
                                            <center>
                                                <div class="form-group col-md-6 mt-4">
                                                    <label class="text mb-2">รหัสนักศึกษา</label>
                                                    <input type="text" name="alumni_stuid" class="form-control" value=" <?= $alumni_stuid ?>" required disabled>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="text mb-2">รหัสผ่านใหม่</label>
                                                    <input type="password" name="np" class="form-control" placeholder="กรอกรหัสผ่านใหม่" required>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="text mb-2">ยืนยันรหัสผ่านใหม่</label>
                                                    <input type="password" name="c_np" class="form-control" placeholder="กรอกยืนยันรหัสผ่านใหม่" required>
                                                </div>
                                                <div class="form-group col-md-6 mb-4">
                                                    <input type="hidden" name="alumni_id" class="form-control" value=" <?= $alumni_id ?>">
                                                    <button type="submit" class="btn btn-success">เปลี่ยนรหัสผ่าน</button>
                                                    <a class="btn btn-primary" href="detail.php?alumni_id=<?= $alumni_id; ?>" role="button">ยกเลิก</a>
                                                </div>
                                            </center>
                                        </form>
                                    </div>
                                </div>
                            <?php  }
                            ?>
            </section>
            </div>
            <?php include_once("../../components/footer.inc.php") ?>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI14J_PNWVd-m0gnUBkjmhoQyNyd7nllA" async defer></script>
            <script src="../../plugins/jquery/jquery.js"></script>
            <script src="../../plugins/bootstrap/bootstrap.min.js"></script>
            <script src="../../plugins/slick/slick.min.js"></script>
            <script src="../../js/custom.js"></script>
            <script src="../../assets/jquery.min.js"></script>
            <script src="../../assets/script.js"></script>
</body>

</html>
<?php
mysqli_close($conn);
?>