<?php
session_start();
require_once("../../controller/config.php");
$alumni_id = $_GET['alumni_id'];
// if(!isset($_SESSION['admin_id']) && $alumni_id != $_SESSION['alumni_id']) {
//     header("Location: index.php");
// }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../../images/logo.png">
    <title>ข้อมูลศิษย์เก่า | มหาวิทยาลัยพายัพ</title>
    <link href="../../plugins/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../plugins/themify-icons/themify-icons.css">
    <link href="../../plugins/slick/slick.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="../../plugins/slick/slick-theme.css" rel="stylesheet">
    <link href="../../css/style.css" rel="stylesheet">
    <link href="../../images/favicon.png" rel="shortcut icon">
    <script src="jquery-3.1.1.min.js" type='text/javascript'></script>
    <link href='bootstrap/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <script src='bootstrap/js/bootstrap.min.js' type='text/javascript'></script>
    <style>
        .a-text {
            color: #3472F7;
        }

        .p-text {
            color: black;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
        }
        .p-text2 {
			color: black;
			font-size: 14px;
			font-weight: 300;
		}
        .p-titletext {
            color: white;

        }

        #img {
            width: 60%;
        }

        .allimg {
            margin: 7px;
            padding: 7px;
            width: 300px;
            height: 300px;
            float: left;
        }

        .a-link{
            color: #4581E0;
        }
        .img-show {

            object-fit: cover;

        }

        /* body {
            background: #eee
        } */

        .card {
            border: none;
            position: relative;
            overflow: hidden;
            border-radius: 8px;
            cursor: pointer;
            background-color: #F8F8FF
        }

        .card:before {
            content: "";
            position: absolute;
            left: 0;
            top: 0;
            width: 4px;
            height: 100%;
            background-color: #7eabf5;
            transform: scaleY(1);
            transition: all 0.5s;
            transform-origin: bottom
        }

        .card:after {
            content: "";
            position: absolute;
            left: 0;
            top: 0;
            width: 4px;
            height: 100%;
            background-color: #4581E0;
            transform: scaleY(0);
            transition: all 0.5s;
            transform-origin: bottom
        }

        .card:hover::after {
            transform: scaleY(1)
        }

        .fonts {
            font-size: 11px
        }

        .fonts-2 {
            font-size: 12px
        }

        .social-list {
            display: flex;
            list-style: none;
            justify-content: center;
            padding: 0
        }

        .social-list li {
            padding: 10px;
            color: #8E24AA;
            font-size: 19px
        }

        .buttons button:nth-child(1) {
            border: 1px solid #8E24AA !important;
            color: #8E24AA;
            height: 40px
        }

        .buttons button:nth-child(1):hover {
            border: 1px solid #8E24AA !important;
            color: #fff;
            height: 40px;
            background-color: #8E24AA
        }

        .buttons button:nth-child(2) {
            border: 1px solid #8E24AA !important;
            background-color: #8E24AA;
            color: #fff;
            height: 40px
        }
    </style>
</head>

<body class="body-wrapper">
    <?php include_once("../../components/navbar.inc.php") ?>
    <!--Homepage Banner-->
    <section class="banner bg-regalumni" id="home">
    </section>
    <section>

        <div class="breadcumb-nav">
            <div class="container">
                <div class="row">
                    <div class="mt-3">
                        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก</a></li>
                                <li class="breadcrumb-item active" aria-current="page">ศิษย์เก่าดีเด่น</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="mb-3 col-12">
                <h5>ประวัติศิษย์เก่าดีเด่น</h5>
                <p class="card-text"><small class="text-muted">ศิษย์เก่าที่ประสบความสำเร็จและได้รับการยอมรับ</small></p>
                <hr align="left" width="100%" color="white">
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row d-flex justify-content-center">
            <?php
            $query = "SELECT * FROM alumni
				LEFT JOIN faculty
           		ON alumni.fac_id = faculty.fac_id
				LEFT JOIN alumni_job
				ON alumni.alumni_id = alumni_job.alumni_id
				WHERE alumni.alumni_id = '$alumni_id'";
            $result2 = mysqli_query($conn, $query);
            while ($row = mysqli_fetch_array($result2)) {
            ?>
                <div class="col-md-7  mb-5">
                    <div class="card p-3 py-4">
                        <div class="text-center"> <img class="rounded-circle img-show" src="../../images/alumni/<?= $row['alumni_img'] ?>" width="200" height="200"> </div>
                        <div class="text-center mt-3">
                            <h5 class="mt-2 mb-3">คุณ<?= $row['alumni_firstname'] ?> <?= $row['alumni_lastname'] ?></h5> <span><?= $row['alumni_job'] ?></span><br><span class="p-text2"><?= $row['alumni_office'] ?></span><br><span><small class="text-muted"><?= $row['fac_name'] ?></small></span>
                            <div class="px-4 mt-4">
                                <p class="fonts-2">“ <?= $row['alumni_description'] ?> “</p>
                            </div>
                            <div class="px-4 mt-4">
                            <p class="text-left">ประสบการณ์ทำงาน / รางวัลความสำเร็จ</p>
                            <hr width="100%" color="black">
                                <p class="fonts-2 text-left"><?= str_replace("\n", "<br>", trim($row['job_exp'])) ?></p>
                            </div>
                            <div class="px-4 mt-1">
                                <p><a class="a-link fonts" href="<?= $row['link'] ?>" target="_blank"><?= $row['link'] ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    </section>
    <?php include_once("../../components/footer.inc.php") ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI14J_PNWVd-m0gnUBkjmhoQyNyd7nllA" async defer></script>
    <script src="../../plugins/jquery/jquery.js"></script>
    <script src="../../plugins/bootstrap/bootstrap.min.js"></script>
    <script src="../../plugins/slick/slick.min.js"></script>
    <script src="../../js/custom.js"></script>
</body>

</html>