<?php
require_once("../auth.inc.php");
$alumni_id = $_GET['alumni_id'];
if(!isset($_SESSION['admin_id']) && $alumni_id != $_SESSION['alumni_id']) {
    header("Location: index.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../../images/logo.png">
    <title>ข้อมูลศิษย์เก่า | มหาวิทยาลัยพายัพ</title>
    <link href="../../plugins/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../plugins/themify-icons/themify-icons.css">
    <link href="../../plugins/slick/slick.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="../../plugins/slick/slick-theme.css" rel="stylesheet">
    <link href="../../css/style.css" rel="stylesheet">
    <link href="../../images/favicon.png" rel="shortcut icon">
    <script src="jquery-3.1.1.min.js" type='text/javascript'></script>
    <link href='bootstrap/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <script src='bootstrap/js/bootstrap.min.js' type='text/javascript'></script>
    <style>
        .a-text {
            color: #3472F7;
        }

        .p-text {
            color: black;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            font-weight: 300;
        }

        .p-titletext {
            color: white;

        }
        .a-link {
            color: #4581E0;
        }
    </style>
</head>

<body class="body-wrapper">
    <?php include_once("../../components/navbar.inc.php") ?>
    <!--Homepage Banner-->
    <section class="banner bg-regalumni" id="home">
    </section>
    <section>
        <div class="breadcumb-nav">
            <div class="container">
                <div class="row">
                    <div class="mt-3">
                        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก</a></li>
                                <li class="breadcrumb-item"><a href="index.php">ทะเบียนศิษย์เก่า</a></li>
                                <li class="breadcrumb-item active" aria-current="page">ข้อมูลศิษย์เก่า</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="mb-3 col-6">
                <h5>ข้อมูลศิษย์เก่า</h5>
            </div>
        </div>
    </div>
    <?php
    if (isset($_GET['alumni_id'])) {
        $alumni_id = $_GET['alumni_id'];

        $query = "SELECT * FROM alumni
            LEFT JOIN faculty
            ON alumni.fac_id = faculty.fac_id
            LEFT JOIN program
            ON alumni.pro_id = program.pro_id
            LEFT JOIN department
            ON alumni.dep_id = department.dep_id
            LEFT JOIN districts
            ON alumni.current_district = districts.district_id
            LEFT JOIN provinces
            ON alumni.current_provinces = provinces.id
            LEFT JOIN amphures
            ON alumni.current_amphure = amphures.amphures_id
            WHERE alumni.alumni_id = '$alumni_id'";
        $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_array($result)) {
            $alumni_id = $row['alumni_id'];
            $alumni_stuid = $row['alumni_stuid'];
            $alumni_fname = $row['alumni_fname'];
            $alumni_firstname = $row['alumni_firstname'];
            $alumni_lastname = $row['alumni_lastname'];
            $alumni_nickname = $row['alumni_nickname'];
            // $alumni_birthday = $row['alumni_birthday'];
            $alumni_gender = $row['alumni_gender'];
            $alumni_phone = $row['alumni_phone'];
            $alumni_email = $row['alumni_email'];
            $alumni_img = $row['alumni_img'];
            $dep_id = $row['dep_id'];
            $fac_id = $row['fac_id'];
            $pro_id = $row['pro_id'];
            $facebooklink = $row['facebooklink'];
            $lineid = $row['lineid'];
            $link = $row['link'];
            $current_amphure = $row['current_amphure'];
            $current_district = $row['current_district'];
            $current_provinces = $row['current_provinces'];
            $current_zipcode = $row['current_zipcode'];
            $current_number = $row['current_number'];
            $modified = $row['modified'];
            $showaddress = $row['showaddress'];
            $showemail = $row['showemail'];
            $showtel = $row['showtel'];
            $showfacebook = $row['showfacebook'];
            $showidline = $row['showidline'];
            $showlink = $row['showlink'];
    ?>
            <section class="blog_are section_padding_0_80">
                <div class="col-12">
                    <div class="card mb-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-4" style="text-align:center">
                                    <div class="post-content mt-4 mb-4">
                                        <img class="img-fluid" src="../../images/alumni/<?= $alumni_img; ?>" width:380px; height:250px; alt="Card image cap">
                                    </div>
                                    <div>
                                        <h6 class="text-dark"><b><?= $alumni_fname . ' ' . $alumni_firstname . ' ' . $alumni_lastname; ?></b></h6>
                                    </div>
                                    <?php if (isset($_SESSION['alumni_id']) && $alumni_id == $_SESSION['alumni_id']) { ?>
                                        <div>
                                            <a class="a-text" href="edit.php?alumni_id=<?= $alumni_id; ?>">แก้ไขข้อมูลส่วนตัว</a>
                                        </div>
                                        <div>
                                            <a class="a-text" href="edit_pwd.php?alumni_id=<?= $alumni_id; ?>">เปลี่ยนรหัสผ่าน</a>
                                        </div>
                                    <?php } ?>
                                    <div class="post-content">
                                        <p><small class="text-muted">แก้ไขล่าสุด <?= substr($modified, 8, 2) . " " . $thaimonth_full[substr($modified, 5, 2) - 1] . " " . substr(substr($modified, 0, 4) + 543, 2, 2) ?> เวลา <?= substr($modified, 11, 8) ?></small></p>
                                    </div>
                                    <br>
                                </div>

                                <div class="col-sm-8">
                                    <div class="post-content">
                                        <div class="card mt-4" style="background:#4581E0">
                                            <p class="p-titletext">ข้อมูลส่วนบุคคล</p>
                                        </div>
                                        <p class="p-text mt-4"><b>ชื่อ : </b><?= $alumni_fname . ' ' . $alumni_firstname; ?> <b>นามสกุล </b><?= $alumni_lastname; ?></p>
                                        <p class="p-text"><b>รหัสนักศึกษา : </b><?= $alumni_stuid; ?></p>
                                        <p class="p-text"><b>ชื่อเล่น : </b><?= $alumni_nickname; ?></p>
                                        <p class="p-text"><b>เพศ : </b><?= $alumni_gender; ?></p>
                                        <!-- <p class="p-text">วัน/เดือน/ปีเกิด : <?= $alumni_birthday ?></p> -->
                                        <!-- <p class="p-text">วัน/เดือน/ปีเกิด : substr($alumni_birthday,8,2)." ". $thaimonth_full[substr($alumni_birthday,5,2)-1]." ". substr(substr($alumni_birthday ,0,4)+543,2,2) </p> -->
                                        <p class="p-text"><b>จบจากสาขา : </b><?= $row["dep_name"]; ?> <b>คณะ : </b><?= $row["fac_name"]; ?> </p>
                                        <p class="p-text"><b>หลักสูตร : </b><?= $row["pro_name_th"]; ?></p>
                                    </div>
                                    <hr width="100%" color="white">
                                    <div class="post-content">
                                        <div class="card" style="background:#4581E0">
                                            <p class="p-titletext">ข้อมูลสำหรับติดต่อศิษย์เก่า</p>
                                        </div>
                                        <?php if ($showaddress) { ?>
                                        <p class="p-text  mt-4"><b>ที่อยู่ปัจจุบัน : บ้านเลขที่ </b><?= $current_number ?> <b>ตำบล </b><?= $row["district_name_th"]; ?> <b>อำเภอ </b><?= $row["amphures_name_th"]; ?>
                                        <b>จังหวัด </b><?= $row["name_th"]; ?> <b>รหัสไปรษณีย์ </b><?= $current_zipcode; ?></p>
                                            <?php } ?>
                                        <?php if ($showtel) { ?>
                                            <p class="p-text"><b>เบอร์โทรศัพท์ : </b><?= $alumni_phone; ?></p>
                                        <?php } ?>
                                        <?php if ($showemail) { ?>
                                            <p class="p-text"><b>อีเมล : </b><?= $alumni_email; ?></p>
                                        <?php } ?>
                                        <?php if ($showfacebook) { ?>
                                            <p class="p-text "><b>Facebook : </b><?= $facebooklink; ?></p>
                                        <?php } ?>
                                        <?php if ($showidline) { ?>
                                            <p class="p-text "><b>Line : </b><?= $lineid; ?></p>
                                        <?php } ?>
                                        <?php if ($showlink) { ?>
                                            <p class="p-text "><b>ลิงค์ส่วนตัว : </b><a class="a-link" href="<?= $link; ?>" target="_blank"><?= $link; ?></a></p>
                                        <?php } ?>
                                    </div>
                                    <hr width="100%" color="white">
                                    <?php
                                    $query2 = "SELECT * FROM alumni_job WHERE alumni_id = '$alumni_id'";
                                    $result2 = mysqli_query($conn, $query2);
                                    while ($row = mysqli_fetch_array($result2)) {
                                        $alumni_job = $row['alumni_job'];
                                        $alumni_job_position = $row['alumni_job_position'];
                                        $alumni_office = $row['alumni_office'];
                                        $job_exp = $row['job_exp'];
                                        if ($row['showdetail']) {
                                    ?>
                                            <div class="post-content">
                                                <div class="card" style="background:#4581E0">
                                                    <p class="p-titletext">ข้อมูลการทำงาน</p>
                                                </div>
                                                <p class="p-text  mt-4"><b>อาชีพ : </b><?= $alumni_job; ?></p>
                                                <p class="p-text "><b>ตำแหน่ง : </b><?= $alumni_job_position; ?></p>
                                                <p class="p-text "><b>สถานที่ทำงาน : </b><br></r><?= str_replace("\n", "<br>", trim($row['alumni_office'])) ?></p>
                                                <p class="p-text "><b>ประสบการณ์ทำงาน / รางวัลความสำเร็จ :</b><br></r> <?= str_replace("\n", "<br>", trim($row['job_exp'])) ?></p>
                                            </div>
                                </div>
                            </div>
                    <?php  }
                                    } ?>
            <?php  }
    } ?>
            </section>
            <?php include_once("../../components/footer.inc.php") ?>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI14J_PNWVd-m0gnUBkjmhoQyNyd7nllA" async defer></script>
            <script src="../../plugins/jquery/jquery.js"></script>
            <script src="../../plugins/bootstrap/bootstrap.min.js"></script>
            <script src="../../plugins/slick/slick.min.js"></script>
            <script src="../../js/custom.js"></script>
</body>

</html>