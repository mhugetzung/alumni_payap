<?php
session_start();
require_once("../../controller/config.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../../images/logo.png">
    <title>ทะเบียนศิษย์เก่า | มหาวิทยาลัยพายัพ</title>
    <link href="../../plugins/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../plugins/themify-icons/themify-icons.css">
    <link href="../../plugins/slick/slick.css" rel="stylesheet">
    <link href="../../plugins/slick/slick-theme.css" rel="stylesheet">
    <link href="../../css/style.css" rel="stylesheet">
    <link href="../../images/favicon.png" rel="shortcut icon">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet"> -->
    <link href="https://cdn.datatables.net/1.11.2/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

</head>

<body class="body-wrapper">
    <?php include_once("../../components/navbar.inc.php") ?>
    <!--Homepage Banner-->
    <section class="banner bg-regalumni" id="home">
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="mt-3">
                    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก</a></li>
                            <li class="breadcrumb-item active" aria-current="page">ทะเบียนศิษย์เก่า</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="mb-3 col-12">
                        <h5>ค้นหาศิษย์เก่าทั้งหมด</h5>
                <hr align="left" width="100%" color="white">
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="mb-3 col-12 ">
                <div class="card mb-4">
                    <!-- Data list table -->
                    <?php
                     $query = "SELECT * FROM alumni
                     LEFT JOIN faculty
                     ON alumni.fac_id = faculty.fac_id
                     LEFT JOIN department
                     ON alumni.dep_id = department.dep_id
                     ORDER BY alumni_id asc";
                    $result = mysqli_query($conn, $query);
                    $arr_alumni = [];
                    $i = 1;
                    if ($result->num_rows > 0) {
                        $arr_alumni = $result->fetch_all(MYSQLI_ASSOC);
                    }
                    ?>
                    <?php if( (isset($_SESSION['admin_id']))){ ?>
                    <div class="card-body table-full-width table-responsive">
                    <!-- <h6 style="color:black;">ค้นหาศิษย์เก่า : </h6> -->
                        <table class="table table-hover table-striped" id="dataTable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>รหัสนักศึกษา</th>
                                    <th>คำนำหน้าชื่อ</th>
                                    <th>ชื่อ</th>
                                    <th>นามสกุล</th>
                                    <th>
                                        <center>คณะวิชา</center>
                                    </th>
                                    <th>
                                        <center>สาขาวิชา</center>
                                    </th>
                                    <th>
                                        <center>เพิ่มเติม</center>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($arr_alumni)) { ?>
                                    <?php foreach ($arr_alumni as $alumni) { ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $alumni['alumni_stuid']; ?></td>
                                            <td><?php echo $alumni['alumni_fname']; ?></td>
                                            <td><?php echo $alumni['alumni_firstname']; ?></td>
                                            <td><?php echo $alumni['alumni_lastname']; ?></td>
                                            <td> <center><?php echo $alumni['fac_name']; ?> </center></td>
                                            <td> <center><?php echo $alumni['dep_name']; ?> </center></td>
                                            <td>
                                                <a href="detail.php?alumni_id=<?= $alumni['alumni_id'] ?>" target="_blank" class="text-primary">
                                                    <center><i class="fa fa-eye" aria-hidden="true"></i></center>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php $i++;
                                    } ?>
                                <?php } ?>
                                <?php  } else { ?>
                                    <div class="card-body table-full-width table-responsive">
                    <!-- <h6 style="color:black;">ค้นหาศิษย์เก่า : </h6> -->
                        <table class="table table-hover table-striped" id="dataTable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>รหัสนักศึกษา</th>
                                    <th>คำนำหน้าชื่อ</th>
                                    <th>ชื่อ</th>
                                    <th>นามสกุล</th>
                                    <th>
                                        <center>คณะวิชา</center>
                                    </th>
                                    <th>
                                        <center>สาขาวิชา</center>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($arr_alumni)) { ?>
                                    <?php foreach ($arr_alumni as $alumni) { ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $alumni['alumni_stuid']; ?></td>
                                            <td><?php echo $alumni['alumni_fname']; ?></td>
                                            <td><?php echo $alumni['alumni_firstname']; ?></td>
                                            <td><?php echo $alumni['alumni_lastname']; ?></td>
                                            <td> <center><?php echo $alumni['fac_name']; ?> </center></td>
                                            <td> <center><?php echo $alumni['dep_name']; ?> </center></td>
                                        </tr>
                                    <?php $i++;
                                    } ?>
                                <?php } ?>
                                    <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>

    <?php include_once("../../components/footer.inc.php") ?>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="../../plugins/jquery/jquery.js"></script>
    <script src="../../plugins/bootstrap/bootstrap.min.js"></script>
    <script src="../../plugins/slick/slick.min.js"></script>
    <script src="../../js/custom.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.2/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.2/js/dataTables.bootstrap4.min.js"></script>
    <script src="../../admin_dashboard/assets/script.js"></script>
</body>

</html>