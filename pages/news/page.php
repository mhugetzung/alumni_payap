<?php
session_start();
require_once("../../controller/config.php");

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../../images/logo.png">
    <title>ข่าวสารประชาสัมพันธ์ | มหาวิทยาลัยพายัพ</title>
    <link href="../../plugins/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../plugins/themify-icons/themify-icons.css">
    <link href="../../plugins/slick/slick.css" rel="stylesheet">
    <link href="../../plugins/slick/slick-theme.css" rel="stylesheet">
    <link href="../../css/style.css" rel="stylesheet">
    <link href="../../images/favicon.png" rel="shortcut icon">
</head>

<body class="body-wrapper">
    <?php include_once("../../components/navbar.inc.php") ?>
    <!--Homepage Banner-->
    <section class="banner bg-1" id="home">
    </section>
    <section>
            <div class="container">
                <div class="row">
                    <div class="mt-3">
                        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="../home/index.php">หน้าแรก</a></li>
                                <li class="breadcrumb-item active" aria-current="page">ข่าวประชาสัมพันธ์และกิจกรรม</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
    </section>
    <div class="container">
			<div class="row">
			<div class="mb-3 col-6"><h5 style="color:black;">ข่าวประชาสัมพันธ์และกิจกรรม</h5></div>
			</div>
	</div>
    <div class="container">
        <div class="row">
            <?php
            if (isset($_GET['news_id'])) {
                $news_id = $_GET['news_id'];
                $select_news = "SELECT * FROM news WHERE news_id = '$news_id'";
                $result = mysqli_query($conn, $select_news);

                while ($row = mysqli_fetch_array($result)) {
                    $news_id = $row['news_id'];
                    $news_title = $row['news_title'];
                    $news_created_at = $row['news_created_at'];
                    $news_img = $row['news_img'];
                    $news_detail = $row['news_detail'];

            ?>
                    <!-- <div class="card mb-3 col-6">.col-12 .col-md-8</div>
					<div class="card mb-3 col-6 ">.col-6 .col-md-4</div> -->
                    <div class="card mb-4">
                        <section class="blog_are section_padding_0_80">
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-12 col-lg-8">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="single-post wow fadeInUp" data-wow-delay=".2s">
                                                    <div class="post-content">
                                                        <h2 class="post-headline"><a style="color:black;"><?= $news_title; ?></a></h2>
                                                    </div>
                                                    <div class="post-content">
                                                        <p><small class="text-muted">เผยแพร่เมื่อ <?= substr($news_created_at,8,2)." ". $thaimonth_full[substr($news_created_at,5,2)-1]." ". substr(substr($news_created_at,0,4)+543,2,2) ?></small></p>
                                                    </div>
                                                    <div class="post-tumb">
                                                        <img class="img-fluid" src="../../images/news/<?= $news_img; ?>" alt="Card image cap">
                                                    </div>
                                                    <div class="post-content mt-4 mb-4 ">
                                                        <p style="color:black; text-align:justify"><?= $news_detail; ?></p>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

            <?php  }
            } ?>
        </div>
    </div>



    <?php include_once("../../components/footer.inc.php") ?>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI14J_PNWVd-m0gnUBkjmhoQyNyd7nllA" async defer></script>
    <script src="../../plugins/jquery/jquery.js"></script>
    <script src="../../plugins/bootstrap/bootstrap.min.js"></script>
    <script src="../../plugins/slick/slick.min.js"></script>
    <script src="../../js/custom.js"></script>
</body>

</html>