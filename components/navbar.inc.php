<nav class="navbar main-nav fixed-top navbar-expand-lg large">
	<div class="container">
		<a class="navbar-brand" href="../home/index.php"><img src="../../images/identity_white.png" width="153" height="50" alt="logo"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="ti-menu text-white"></span>
		</button>
		<?php
		if (!isset($_GET['lang'])) {
			//  default language
			include('../../languages/th.inc.php');
		} else {
			// user selected language
			$lang = $_GET['lang'];
			if ($lang == "th") {
				include('../../languages/th.inc.php');
			} else {
				include('../../languages/en.inc.php');
			}
		}
		?>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link scrollTo" href="../../index.php"><?=HOME?></a>
				</li>
				<li class="nav-item">
					<a class="nav-link scrollTo" href="#about"><?=ABOUT?></a>
				</li>
				<li class="nav-item">
					<a class="nav-link scrollTo" href="#alumniaward"><?=DistinguishedAlumni?></a>
				</li>
				<li class="nav-item">
					<a class="nav-link scrollTo" href="../../pages/alumni/index.php"><?=ALUMNI?></a>
				</li>
				<li class="nav-item">
					<?php if (isset($_SESSION['admin_id'])) { ?>
				<li class="nav-item dropdown no-arrow">
					<a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="mr-2 d-none d-lg-inline text-gray-600 regular"><?= $_SESSION['name']; ?></span>
					</a>
					<!-- Dropdown - User Information -->
					<div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
						<div class="dropdown-menu"></div>
						<a class="dropdown-item" href="../../admin_dashboard/pages/index.php">
							<i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
							แดชบอร์ด
						</a>
						<a class="dropdown-item" href="../../logout.php">
							<i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
							ออกจากระบบ
						</a>
					</div>
				</li>
			<?php } else if (isset($_SESSION['alumni_id'])) { ?>
				<li class="nav-item dropdown no-arrow">
					<a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="mr-2 d-none d-lg-inline text-gray-600 regular"><?= $_SESSION['username']; ?></span>
					</a>
					<!-- Dropdown - User Information -->
					<?php
						$sql_alumni = "SELECT *
						FROM alumni WHERE alumni_id = '" . $_SESSION["alumni_id"] . "'";
						$result = mysqli_query($conn, $sql_alumni);
						$alumni_profile = mysqli_fetch_assoc($result);
					?>
					<div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
						<div class="dropdown-menu"></div>
						<a class="dropdown-item" href="../alumni/detail.php?alumni_id=<?= $alumni_profile['alumni_id'] ?>">
							<i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
							แก้ไขข้อมูลส่วนตัว
						</a>
						<a class="dropdown-item" href="../../logout.php">
							<i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
							ออกจากระบบ
						</a>
					</div>

				</li>
			<?php } else { ?>
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<?=LOGIN?>
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="../../admin_dashboard/pages/auth/login.php">เข้าสู่ระบบสำหรับบุคลากร</a>
					<a class="dropdown-item" href="../../login/login.php">เข้าสู่ระบบสำหรับศิษย์เก่า</a>
				</div>
			<?php } ?>
			</li>
			<li class="nav-item">
				<a class="nav-link scrollTo">|</a>
			</li>
			<li class="nav-item">
				<a class="navbar-brand" href="?lang=th"><img src="../../images/th.png" width="30" height="20" alt="logo"></a>
			</li>

			<li class="nav-item">
				<a class="navbar-brand" href="?lang=en"><img src="../../images/en.png" width="30" height="20" alt="logo"></a>
			</li>
			</ul>
		</div>
	</div>
</nav>