<footer class="footer-main">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 mr-auto">

        <div class="copyright">
          <h6><?=FACULTY;?></h6>
          <a href="https://pharmacy.payap.ac.th/info/home/" target="_blank"><?=FACULTYOFPHARMACY;?></a>
          <br> <a href="https://arts.payap.ac.th/home/" target="_blank"><?=FACULTYOFHUMANITIESANDCOMMUNICATIONARTS;?></a>
          <br> <a href="https://law.payap.ac.th/home/" target="_blank"><?=FACULTYOFLAW;?></a>
          <br> <a href="https://business.payap.ac.th/home/" target="_blank"><?=FACULTYOFBUSINESSADMINISTRATION;?></a>
          <br> <a href="https://account.payap.ac.th/info/home/" target="_blank"><?=FACULTYOFACCOUNTINGANDDIGITALBUSINESS?></a>
          <br> <a href="https://nurse.payap.ac.th/info/" target="_blank"><?=MCCORMICKFACULTYOFNURSING;?></a>
          <br> <a href="https://mcd.payap.ac.th/info/" target="_blank"><?=MCGILVARYOFDIVINITY?></a>
          <br> <a href="https://music.payap.ac.th/info/" target="_blank"><?=DURIYASILPCOLLEGEOFMUSIC?></a>
          <br> <a href="https://ic.payap.ac.th" target="_blank"><?=THEINTERNATIONALCOLLEGE;?></a>
        </div>
      </div>

      <div class="col-lg-4 mr-auto">

        <div class="copyright">
          <h6><?=CONTACTUS;?></h6>
          <p>| <?=PAYAPMAEKAO;?> <br>
            | <?=PAYAPKAEWNAWARAT;?></p>
          <br>
          <p><?=TEL;?><br>
            <?=EMAILPAYAP;?></p>
        </div>
        <br><br>
        <span style="font-size: 14px; color: #ffffff;">Copyright © <script>
            document.write(new Date().getFullYear())
          </script> <a href="https://www.payap.ac.th/_public/" target="_blank">Payap University</a>
        </span>
      </div>
      <div class="col-lg-4 text-lg-right">
        <div class="copyright">
          <h6><?=OFFICE;?></h6>
          <a href="https://academic.payap.ac.th/home/" target="_blank"><?=OFFICEOFACADEMICANDRESEARCHDEPARTMENT;?></a>
          <br> <a href="https://news.payap.ac.th/home/" target="_blank"><?=OFFICEOFMARKETINGANDCORPORATECOMMUNICATION;?></a>
          <br> <a href="https://ia.payap.ac.th/" target="_blank"><?=PAYAPINTERNATIONALAFFAIRS;?></a>
          <br> <a href="http://reso.payap.ac.th/" target="_blank"><?=OFFICEOFREGISTRARANDEDUCATIONSERVICE;?></a>
          <br> <a href="https://pdf.payap.ac.th/home/" target="_blank"><?=OFFICEOFFINANCIAL;?></a>
          <br> <a href="https://sa.payap.ac.th/home/" target="_blank"><?=STUDENTAFFAIRS?></a>
          <br> <a href="https://www.payap.ac.th/_public/Departments.html" target="_blank"><?=ALLDEP;?></a>

        </div>
        <br><br><br>
        <!-- Social Icons -->
        <ul class="social-icons list-inline">
          <li class="list-inline-item">
            <a target="_blank" href="https://www.facebook.com/pyuofficial"><i class="text-primary ti-facebook"></i></a>
          </li>
          <li class="list-inline-item">
            <a target="_blank" href="https://twitter.com/PayapChiangmai"><i class="text-primary ti-twitter-alt"></i></a>
          </li>
          <li class="list-inline-item">
            <a target="_blank" href="https://www.youtube.com/channel/UCJNf5HlDSx6cI_JHIghghaQ/videos"><i class="text-primary ti-linkedin"></i></a>
          </li>
          <li class="list-inline-item">
            <a target="_blank" href="https://www.instagram.com/pyuofficial/"><i class="text-primary ti-instagram"></i></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>